export interface User {
  firstName?: string;
  lastName?: string;
  email: string;
  password: string;
}

export interface GetUsersQuery {
  page?: number;
  limit?: number;
}

export interface GetProductQuery {
  page?: number;
  limit?: number;
  search?: string;
}

interface ListUser {
  id: string;
  email: string;
  firstName: string;
  lastName: string;
  admin: boolean;
  createdAt?: string;
  phoneNumber?: string;
  address?: string;
}

interface Role {
  id: string;
  role: boolean;
}

interface UpdateProfile {
  id: string;
  firstName: string;
  lastName: string;
  phoneNumber: string;
  address: string;
}
