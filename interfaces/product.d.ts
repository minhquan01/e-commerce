export interface ListProduct {
    id: string
    url: string
    productDetail: {
        id: string
        camera: string
        color: string[]
        idProduct: string
        imageDetail: string[]
        memory: string[]
        name: string
        price: string
        screen: string
    }
}

export interface ProductManage {
    id: string
    url: string
    name: string
    price: string
}

export interface ProductUpload {
    idProdEdit?: string
    urlProd: string
    nameProd: string
    priceProd: string
    memoryProd: string[]
    screenProd: string
    cameraProd: string
    listColor: string[]
    listImage: string[]
}
