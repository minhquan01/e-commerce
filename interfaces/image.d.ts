export interface GetBannerQuery {
  page?: number;
  limit?: number;
}

export interface BannerSlider {
  id: string;
  url: string;
  createdAt: string;
  publish: boolean;
}

export interface BannerMobileProps {
  id: string;
  url: string;
  createdAt: string;
  name: string;
  description: string;
}

export interface UploadBannerMobile {
  url: string;
  name: string;
  des: string;
}

export interface UploadBannerTv {
  url: string;
  name: string;
}
