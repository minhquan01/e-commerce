interface ChildrenProps {
  children?: React.ReactNode;
}

export interface MessValidatorRegis {
  email: string;
  firstName: string;
  lastName: string;
  userName: string;
  password: string;
  password2: string;
}

export interface FeaturedProps {
  title: string;
  color?: string;
  titleBtn?: string;
  categorys:
    | [
        {
          name: string;
          images: [string];
          titleImg?: string | undefined;
          descImg?: string | undefined;
        }
      ]
    | {
        name: string;
        images: string[];
        titleImg?: string | undefined;
        descImg?: string | undefined;
      }[];
}

export interface FeaturedMobileProps {
  title: string;
  categorys:
    | [
        {
          [x: string]: any;
          name: string;
          url: string;
          description: string;
          id: string;
          createdAt: string;
          updatedAt: string;
        }
      ]
    | never[];
}

export interface FeaturedTvProps {
  title: string;
  titleBtn: string;
  color: string;
  categorys:
    | [
        {
          name: string;
          url: string;
          id: string;
          createdAt: string;
          updatedAt: string;
        }
      ]
    | never[];
}

export interface ExploreProps {
  explores:
    | [
        {
          name: string;
          url: string;
        }
      ]
    | never[];
}

export interface MenuDropProps {
  title?: string;
  options: string[];
  children?: React.ReactNode;
  className?: string;
}

export interface UseHrefProps {
  title?: string;
  options: { title: string; href: string }[];
  children?: React.ReactNode;
  className?: string;
}

export interface ContentAdminProps {
  children: React.ReactNode;
  title?: string;
}

export type RootState = ReturnType<typeof rootReducer>;
