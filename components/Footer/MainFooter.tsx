import React from "react";
import { AiOutlineInstagram, AiFillYoutube } from "react-icons/ai";
import { BsFacebook } from "react-icons/bs";
import { useState } from "react";

const footerCategorys = [
  {
    title: "Sản Phẩm & Dịch Vụ",
    listCategorys: [
      "Điện thoại thông minh",
      "Máy Tính Bảng",
      "Thiết Bị Âm Thanh",
      "Thiết Bị Đeo",
      "Smart Switch",
      "Phụ Kiện",
      "TVs",
      "Lifestyle TVs",
      "Thiết Bị Nghe Nhìn",
      "Tủ Lạnh",
      "Máy Giặt & Máy Sấy",
      "Giải Pháp Không Khí",
      "Gia Dụng Nhà Bếp",
      "Màn Hình",
    ],
  },
  {
    title: "Mua Trực Tuyến",
    listCategorys: [
      "Ưu Đãi Sinh Viên",
      "Ưu Đãi Nhân Viên Đối Tác",
      "Ưu Đãi Độc Quyền",
      "Cửa hàng trực tuyến doanh nghiệp",
      "Samsung 68",
      "Cửa Hàng Trải Nghiệm Samsung",
      "Câu hỏi thường gặp",
      "Tìm Cửa Hàng",
      "Điểm tư vấn và nhận hàng trực tiếp",
      "Samsung Rewards",
      "Khám Phá",
    ],
  },
  {
    title: "Bạn Cần Hỗ Trợ?",
    listCategorys: [
      "Đặt hẹn lịch sửa chữa",
      "Tư Vấn Trực Tuyến ",
      "Thư điện tử ",
      "Điện Thoại",
      "Trung Tâm Bảo Hành",
      "Ngôn ngữ ký hiệu",
      "Gửi ý kiến phản hồi ",
      "Gửi thư cho Ban Giám đốc ",
    ],
  },
  {
    title: "Tài khoản & Cộng đồng",
    listCategorys: [
      "Tài Khoản Của Tôi",
      "Đơn Hàng",
      "Danh Sách Yêu Thích",
      "Samsung Members",
    ],
  },
  {
    title: "Giới thiệu về chúng tôi",
    listCategorys: [
      "Thông tin về Công ty",
      "Lĩnh vực kinh doanh",
      "Nhận diện thương hiệu",
      "Nghề nghiệp",
      "Quan hệ với nhà đầu tư ",
      "Newsroom ",
      "Đạo đức",
      "Samsung Design ",
    ],
  },
];

const MainFooter = () => {
  const [indexList, setIndexList] = useState(0);

  return (
    <div>
      <div className="ml-5 md:ml-0 md:grid md:grid-cols-5 mb-5 border-b ">
        {footerCategorys.map((footerCategory, index) => (
          <div key={index} className=" col-span-1 ">
            <h1
              className="font-semibold text-lg"
              onClick={() => setIndexList(index + 1)}
            >
              {footerCategory.title}
            </h1>
            {footerCategory.listCategorys.map((listCategory, indexList) => (
              <p
                className=" cursor-pointer text-sm opacity-90 my-4 hover:text-blue-500 "
                key={indexList}
              >
                {listCategory}
              </p>
            ))}
          </div>
        ))}
      </div>
      <div className="flex justify-between flex-col md:flex-row text-xs mb-5 pt-2">
        <div className="flex flex-col md:flex-row space-y-4 md:space-y-0 justify-center mb-4 items-center space-x-4">
          <h1 className=" font-bold pr-4">Việt Nam/Tiếng Việt</h1>
          <div className="flex items-center space-x-4">
            <p>Điều Khoản & Điều Kiện</p>
            <p>Bảo Mật</p>
            <p>Pháp Lý</p>
          </div>
        </div>
        <div className="flex flex-col md:flex-row justify-center items-center space-x-4">
          <p>Kết Nối Với Chúng Tôi</p>
          <div className="text-2xl mt-2 md:mt-0 flex items-center md:space-x-4">
            <BsFacebook className="mr-2 md:mr-0" />
            <AiFillYoutube className="text-3xl mr-2 md:mr-0" />
            <AiOutlineInstagram className="mr-2 md:mr-0" />
          </div>
        </div>
      </div>
    </div>
  );
};

export default MainFooter;
