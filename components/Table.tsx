import React, { ReactNode } from "react";
import LoadingPage from "./loading/LoadingPage";

interface TableProps {
  /**
   * Data record array to be displayed
   */
  dataSource: (ReactNode | string)[][];
  /**
   * Columns of table
   */
  columns: (ReactNode | string)[];
  /**
   * The loading state of the table
   */
  loading?: boolean;
}

const Table = ({ dataSource, columns, loading }: TableProps) => {
  return (
    <div>
      {loading && <LoadingPage />}
      <div className="mt-8 flex flex-col">
        <div className="-my-2 -mx-4 overflow-x-auto sm:-mx-6 lg:-mx-8">
          <div className="inline-block min-w-full py-2 align-middle md:px-6 lg:px-8">
            <div className="overflow-hidden shadow ring-1 ring-black ring-opacity-5 md:rounded-lg">
              <table className="min-w-full divide-y divide-gray-300">
                <thead className="bg-gray-50">
                  <tr>
                    {columns.map((column, index) => (
                      <th
                        key={index}
                        scope="col"
                        className="py-3.5 pl-4 pr-3 w-fit text-left text-sm font-semibold text-gray-900"
                      >
                        {column}
                      </th>
                    ))}
                  </tr>
                </thead>
                <tbody className="divide-y divide-gray-200 bg-white">
                  {dataSource.map((row, index) => (
                    <tr key={index}>
                      {row.map((item, idx) => (
                        <td
                          className="whitespace-nowrap w-fit px-3 py-4 text-sm text-gray-500"
                          key={idx}
                        >
                          <div className="flex space-x-1">{item}</div>
                        </td>
                      ))}
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Table;
