import { useEffect } from 'react';
import { useRouter } from 'next/router';
import Cookies from 'js-cookie';
import jwt_decode from "jwt-decode";
export { RouteGuard };


function RouteGuard({ children }) {
    const cookie = Cookies.get('token')
    const router = useRouter();

    useEffect(() => {
        authCheck(router.asPath);
    }, [router.asPath]);

    function authCheck(url) {
        // redirect to login page if accessing a private page and not logged in 
        const publicPages = ["/login", "/register", "/", "/product"];
        const userPages = ["/cart", '/profile'];
        const adminPages = ["/dashboard", "/banner", "/account",'/product-management'];
        const path = url.split('?')[0];

        if (cookie) {
            const token = jwt_decode(cookie);
            if (token.admin === true && userPages.includes(path)) {
                router.push({
                    pathname: '/dashboard',
                });
            }
            if (token.admin === false && adminPages.includes(path)) {
                router.push({
                    pathname: '/',
                });
            }
        }
        else if (!cookie && userPages.includes(path) || adminPages.includes(path)) {
            router.push({
                pathname: '/login',
            });
        }
    }

    return children
}
