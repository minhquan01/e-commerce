import React from "react";
import { useState, useEffect } from "react";
import { Swiper, SwiperSlide } from "swiper/react";

import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";

import { Pagination } from "swiper";
import { ExploreProps } from "../interfaces";

const Explore = ({ explores }: ExploreProps) => {
  const [indexEx, setIndexEx] = useState(0);

  return (
    <div className="mt-20">
      <h1 className="font-bold text-2xl md:flex md:justify-center md:items-center space-x-2 md:text-4xl text-center my-10">
        <p>Explore</p>
        <p>#DoWhatYouCant</p>
      </h1>
      <div className="hidden md:grid grid-cols-2 px-10 gap-10 ">
        <div className="col-span-1 max-h-screen overflow-hidden rounded-xl">
          <img
            className=" hover:scale-[1.1] transition-all object-cover cursor-pointer"
            src={explores[indexEx]?.url}
            alt=""
          />
        </div>
        <div className="col-span-1">
          {explores.map((explore, index) => (
            <div
              onMouseOver={() => setIndexEx(index)}
              key={index}
              className={`flex space-x-16 my-10 cursor-default row-span-1 items-center  transition-all ${
                indexEx === index ? "border-t-[3px] mt-4 border-black" : ""
              }`}
            >
              <p className="font-bold">0{index + 1}</p>
              <div
                className={` flex flex-col space-y-4 ${
                  indexEx === index ? "border-b-2 pb-5" : ""
                }`}
              >
                <h2 className="font-semibold text-3xl pt-4">{explore.name}</h2>
                {indexEx === index ? (
                  <p className="font-semibold text-base max-w-fit border-b-2 border-black cursor-pointer">
                    Tìm hiểu thêm
                  </p>
                ) : (
                  <></>
                )}
              </div>
            </div>
          ))}
        </div>
      </div>
      <div className="md:hidden">
        <Swiper
          slidesPerView={"auto"}
          spaceBetween={30}
          pagination={{
            clickable: true,
          }}
          modules={[Pagination]}
          className="mySwiper"
        >
          {explores.map((explore, index) => (
            <SwiperSlide key={index}>
              <div className="w-screen mb-10">
                <img
                  src={explore.url}
                  className="rounded-xl w-[90%] mx-auto"
                  alt=""
                />
                <div className="flex flex-col justify-center items-center">
                  <h2 className="text-base font-semibold mt-2 w-4/5 text-center mx-auto">
                    {explore.name}
                  </h2>
                  <p className="font-medium max-w-fit text-sm text-center border-b-[1px] border-black cursor-pointer">
                    Tìm hiểu thêm
                  </p>
                </div>
              </div>
            </SwiperSlide>
          ))}
        </Swiper>
      </div>
    </div>
  );
};

export default Explore;
