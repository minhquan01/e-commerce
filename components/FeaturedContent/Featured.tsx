/* eslint-disable @next/next/no-img-element */
/* eslint-disable jsx-a11y/alt-text */
import React, { useState } from "react";
import { FeaturedProps } from "../../interfaces";

const Featured = ({ title, categorys }: FeaturedProps) => {
  const [indexOf, setIndexOf] = useState(0);

  return (
    <div className="mt-8 md:mt-14">
      <h1 className="font-bold text-2xl md:text-4xl text-center mb-5 md:mb-10">
        {title}
      </h1>
      <div
        className={`${
          categorys[indexOf]?.images.length === 5
            ? "bg-slate-100 py-2 px-6"
            : ""
        }`}
      >
        <div className="flex flex-wrap justify-center space-x-4 py-4 md:space-x-10 md:py-6">
          {categorys.map((category, index) => (
            <div key={category.name} onClick={() => setIndexOf(index)}>
              <p
                className={`font-semibold text-xs md:text-lg cursor-pointer md:mt-0 mt-3 ${
                  index === indexOf
                    ? "border-b-2 border-black"
                    : "hover:opacity-60"
                }`}
              >
                {category.name}
              </p>
            </div>
          ))}
        </div>
        <div
          className={`${
            categorys[indexOf]?.images.length === 5
              ? "grid md:grid-cols-4 md:grid-rows-4 md:grid-flow-col grid-cols-2 md:gap-2 gap-4 rounded-xl"
              : "flex justify-center items-center"
          }`}
        >
          {categorys[indexOf]?.images.map((image, indexImg) => (
            <img
              key={indexImg}
              src={image}
              className={`rounded-xl hover:scale-[1.01] cursor-pointer transition-all ${
                indexImg === 0
                  ? "col-span-2 row-span-4 h-[98%] my-auto"
                  : "col-span-1 row-span-2 my-[7px]"
              } `}
            />
          ))}
        </div>
        <div className="text-center">
          <h1 className="text-2xl mt-4 md:mt-0 md:text-5xl font-semibold">
            {categorys[indexOf]?.titleImg}
          </h1>
          <p className="md:my-5 my-2 md:text-xl">
            {categorys[indexOf]?.descImg}
          </p>
          {categorys[indexOf]?.images.length === 1 ? (
            <button className="bg-black text-white font-semibold text-sm px-4 py-2 hover:opacity-80 rounded-full">
              Mua ngay
            </button>
          ) : (
            <></>
          )}
        </div>
      </div>
    </div>
  );
};

export default Featured;
