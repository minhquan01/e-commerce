/* eslint-disable @next/next/no-img-element */
/* eslint-disable jsx-a11y/alt-text */
import React, { useState } from "react";
import { FeaturedProps, FeaturedTvProps } from "../../interfaces";

const FeaturedImg = ({
  title,
  titleBtn,
  color,
  categorys,
}: FeaturedTvProps) => {
  const [indexOf, setIndexOf] = useState(0);

  return (
    <div className={`mt-20 text-${color} relative`}>
      <div className="absolute inset-0 pt-10 md:pt-12">
        <h1 className="font-bold text-2xl md:text-4xl text-center md:mb-10">
          {title}
        </h1>
        <div>
          <div className="flex flex-wrap justify-center space-x-2 py-4 md:space-x-10 md:py-6 px-1">
            {categorys.map((category, index) => (
              <div key={category.name}>
                <p
                  onClick={() => setIndexOf(index)}
                  className={`font-semibold text-xs md:text-lg cursor-pointer md:mt-0 mt-3 ${
                    index === indexOf
                      ? `border-${color} border-b-2 `
                      : `hover:opacity-90 hover:bg-${color} hover:bg-opacity-[0.03] px-5 rounded-full`
                  }`}
                >
                  {category.name}
                </p>
              </div>
            ))}
          </div>
        </div>
      </div>
      <div className="flex justify-center items-center">
        <img
          src={categorys[indexOf]?.url}
          className="cursor-pointer transition-all h-screen object-cover w-full col-span-1 row-span-2 my-[7px]"
        />
      </div>
      <div className="text-center absolute bottom-10 right-0 left-0 ">
        <h1 className="text-2xl md:text-5xl font-semibold">
          {categorys[indexOf]?.name}
        </h1>

        <div className="flex justify-center md:flex-row flex-col-reverse items-center md:space-x-7 mt-8">
          <p
            className={`cursor-pointer border-b-2 border-${color} text-sm font-medium mt-2 md:mt-0`}
          >
            Tìm hiểu thêm
          </p>
          <button
            className={`bg-${color} px-5 py-3 hover:opacity-80 rounded-full`}
          >
            <p className={`text-${color} font-semibold text-sm invert`}>
              {titleBtn}
            </p>
          </button>
        </div>
      </div>
    </div>
  );
};

export default FeaturedImg;
