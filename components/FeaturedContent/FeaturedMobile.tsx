/* eslint-disable @next/next/no-img-element */
/* eslint-disable jsx-a11y/alt-text */
import React, { useState } from "react";
import { FeaturedMobileProps } from "../../interfaces";

const FeaturedMobile = ({ title, categorys }: FeaturedMobileProps) => {
  const [indexOf, setIndexOf] = useState(0);

  return (
    <div className="mt-8 md:mt-14">
      <h1 className="font-bold text-2xl md:text-4xl text-center mb-5 md:mb-10">
        {title}
      </h1>
      <div>
        <div className="flex flex-wrap justify-center space-x-4 py-4 md:space-x-10 md:py-6">
          {categorys.map((category, index) => (
            <div key={category.name}>
              <p
                onClick={() => setIndexOf(index)}
                className={`font-semibold text-xs md:text-lg cursor-pointer md:mt-0 my-3 ${
                  index === indexOf
                    ? "border-b-2 border-black"
                    : "hover:opacity-60"
                }`}
              >
                {category.name}
              </p>
            </div>
          ))}
          <div className="flex justify-center items-center">
            <img
              src={categorys[indexOf]?.url}
              className="rounded-xl md:w-2/3 hover:scale-[1.01] cursor-pointer transition-all col-span-1 row-span-2 my-[7px]"
            />
          </div>
          <div className="text-center">
            <h1 className="text-2xl mt-4 md:mt-0 md:text-5xl font-semibold">
              {categorys[indexOf]?.name}
            </h1>
            <p className="md:my-5 my-2 md:text-xl">
              {categorys[indexOf]?.description}
            </p>

            <button className="bg-black text-white font-semibold text-sm px-4 py-2 hover:opacity-80 rounded-full">
              Mua ngay
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FeaturedMobile;
