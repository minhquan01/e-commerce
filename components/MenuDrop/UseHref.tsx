import React, { Fragment } from "react";
import { Menu, Transition } from "@headlessui/react";
import { IoIosArrowDown } from "react-icons/io";
import { UseHrefProps } from "../../interfaces";
import Link from "next/link";
import Cookies from "js-cookie";
import { useEffect } from "react";
import { decodeToken } from "react-jwt";
import { useState } from "react";
import { FiUser } from "react-icons/fi";
import { useRouter } from "next/router";
import { userLogin } from "../../lib/userLogin";

const UseHref = ({ title, children, options, className }: UseHrefProps) => {
  const [fullName, setFullName] = useState<string>("");
  const router = useRouter();

  const token = Cookies.get("token");

  const user: any = userLogin();

  const handleSignOut = () => {
    router.push("/login");
    Cookies.remove("token");
  };

  useEffect(() => {
    if (token) {
      const jwtToken: any = decodeToken(token);
      const fristName = jwtToken.firstName
        ?.substring(0, 1)
        .concat(
          jwtToken.firstName
            ?.substring(1, jwtToken.firstName.length)
            .toLowerCase()
        );
      const lastName = jwtToken.lastName
        ?.substring(0, 1)
        .concat(
          jwtToken.lastName
            ?.substring(1, jwtToken.lastName.length)
            .toLowerCase()
        );
      setFullName(fristName + " " + lastName);
    }
  }, [token]);

  return (
    <Menu as="div" className="relative">
      <div className="font-semibold ">
        <Menu.Button className="relative flex items-center">
          {children ? children : title}
          {title ? <IoIosArrowDown className="ml-2" /> : <></>}
        </Menu.Button>
      </div>
      <Transition
        as={Fragment}
        enter="transition ease-out duration-100"
        enterFrom="transform opacity-0 scale-95"
        enterTo="transform opacity-100 scale-100"
        leave="transition ease-in duration-75"
        leaveFrom="transform opacity-100 scale-100"
        leaveTo="transform opacity-0 scale-95"
      >
        <Menu.Items
          className={`absolute z-10 mt-2 min-w-fit origin-top-right rounded-md bg-white shadow-xl ${className}`}
        >
          <div className="my-2 px-4">
            {token ? (
              <Menu.Item>
                {({ active }) => (
                  <Link href={`/profile/${user.id}`}>
                    <div className="text-base py-2 px-3 flex space-x-2 border-b hover:opacity-100 text-black opacity-80 cursor-pointer items-center rounded-sm">
                      <div className="bg-gray-200 w-8 h-8 rounded-full flex items-center justify-center">
                        <FiUser />
                      </div>
                      <p className="text-base font-semibold">{fullName}</p>
                    </div>
                  </Link>
                )}
              </Menu.Item>
            ) : (
              <></>
            )}
            {options.map((option) => (
              <Menu.Item key={option.title}>
                {({ active }) => (
                  <Link href={option.href}>
                    <div className="text-base py-2 px-3 flex hover:opacity-100 text-black opacity-80 cursor-pointer items-center rounded-sm">
                      <p>{option.title}</p>
                    </div>
                  </Link>
                )}
              </Menu.Item>
            ))}
            {token ? (
              <Menu.Item>
                <div
                  onClick={handleSignOut}
                  className="text-base py-2 px-3 flex space-x-2 border-t hover:text-red-500 text-black opacity-80 cursor-pointer items-center rounded-sm"
                >
                  <p className="font-medium">Đăng xuất</p>
                </div>
              </Menu.Item>
            ) : (
              <></>
            )}
          </div>
        </Menu.Items>
      </Transition>
    </Menu>
  );
};

export default UseHref;
