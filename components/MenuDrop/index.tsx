import React, { Fragment } from "react";
import { Menu, Transition } from "@headlessui/react";
import { IoIosArrowDown } from "react-icons/io";
import { AiOutlineCheck } from "react-icons/ai";
import { useState } from "react";
import { MenuDropProps } from "../../interfaces";

const MenuDrop = ({ title, options }: MenuDropProps) => {
  const [currenOption, setCurrenOption] = useState<string>();

  return (
    <Menu as="div" className="relative">
      <div className="font-semibold ">
        <Menu.Button className="relative flex items-center">
          {title}
          <IoIosArrowDown className="ml-2" />
        </Menu.Button>
      </div>
      <Transition
        as={Fragment}
        enter="transition ease-out duration-100"
        enterFrom="transform opacity-0 scale-95"
        enterTo="transform opacity-100 scale-100"
        leave="transition ease-in duration-75"
        leaveFrom="transform opacity-100 scale-100"
        leaveTo="transform opacity-0 scale-95"
      >
        <Menu.Items className="absolute md:right-7 left-0 top-6 z-10 mt-2 max-w-fit origin-top-right rounded-md bg-white shadow-lg">
          <div className="my-2">
            {options.map((option) => (
              <Menu.Item key={option}>
                {({ active }) => (
                  <div
                    onClick={() => setCurrenOption(option)}
                    className={`hover:bg-gray-100 text-base py-2 px-3 flex cursor-pointer items-center rounded-sm ${
                      currenOption === option ? "text-black" : "text-gray-500"
                    }`}
                  >
                    <p>{option}</p>
                    {currenOption === option ? (
                      <AiOutlineCheck className="ml-2" />
                    ) : (
                      <></>
                    )}
                  </div>
                )}
              </Menu.Item>
            ))}
          </div>
        </Menu.Items>
      </Transition>
    </Menu>
  );
};

export default MenuDrop;
