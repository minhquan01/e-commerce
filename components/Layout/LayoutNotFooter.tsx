import React from "react";
import { ChildrenProps } from "../../interfaces";
import Header from "../Header";
import BtnBackToTop from "../Scroll/BtnBackToTop";

const LayoutNotFooter = ({ children }: ChildrenProps) => {
  return (
    <div className="overflow-hidden pb-10">
      <header>
        <Header />
      </header>
      <main>
        <div className="px-4 md:max-w-screen-2xl mx-auto md:px-4 sm:px-6 lg:px-8">
          {children}
        </div>
      </main>

      <BtnBackToTop />
    </div>
  );
};

export default LayoutNotFooter;
