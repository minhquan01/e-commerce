import React from "react";
import { ChildrenProps } from "../../interfaces";
import MainFooter from "../Footer/MainFooter";
import Header from "../Header";
import BtnBackToTop from "../Scroll/BtnBackToTop";

const Mainlayout = ({ children }: ChildrenProps) => {
  return (
    <div className="overflow-hidden">
      <header>
        <Header />
      </header>
      <main>
        <div className="px-4 md:max-w-screen-2xl mx-auto md:px-4 sm:px-6 lg:px-8">
          {children}
          <footer className="mt-10">
            <MainFooter />
          </footer>
        </div>
      </main>

      <BtnBackToTop />
    </div>
  );
};

export default Mainlayout;
