import React from "react";
import { ContentAdminProps } from "../../interfaces";
import { CustomHeader } from "../Header/CustomHeader";

const ContentAdmin = ({ children, title }: ContentAdminProps) => {
  return (
    <>
      <CustomHeader>
        <title>{title}</title>
      </CustomHeader>
      <h1 className="text-2xl font-medium my-3">{title}</h1>
      <div>{children}</div>
    </>
  );
};

export default ContentAdmin;
