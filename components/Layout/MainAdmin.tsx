import React from "react";
import { ChildrenProps } from "../../interfaces";
import HeaderAdmin from "../Header/HeaderAdmin";
import MainMenu from "../NavBar/MainMenu";

const MainAdmin = ({ children }: ChildrenProps) => {
  return (
    <div>
      <div className=" w-screen flex justify-center items-center h-screen">
        <div className="h-full w-full overflow-hidden bg-slate-100 ">
          <div className="h-[11%]">
            <HeaderAdmin />
          </div>
          <div className="grid h-full grid-cols-5 gap-x-5 px-10">
            <div className="col-span-1">
              <div className="fixed top-4 w-72 left-8 h-[95%] bg-white rounded-lg shadow-lg">
                <MainMenu />
              </div>
            </div>
            <main className="h-[89%] overflow-y-scroll col-span-4">
              <div className="px-4 pb-10 mx-auto md:px-4 sm:px-6 lg:px-8 ">
                {children}
              </div>
            </main>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MainAdmin;
