import { Disclosure } from "@headlessui/react";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

interface GroupItemProps {
  href?: string;
  label: string;
  icon: (props: React.SVGProps<SVGSVGElement>) => JSX.Element;
  children?:
    | {
        href: string;
        label: string;
      }[]
    | undefined;
}

const GroupItem = ({ item }: { item: any }): JSX.Element => {
  const [isOpen, setIsOpen] = useState(false);
  const router = useRouter();
  const path = router.pathname;
  useEffect(() => {
    if (item.children.some((child: any) => child.href === path))
      setIsOpen(true);
    else setIsOpen(false);
  }, [path]);

  return (
    <Disclosure
      as="div"
      key={item.label}
      className={`group hover:no-underline text-sm font-medium rounded-md text-gray-400 `}
    >
      <div onClick={() => setIsOpen(!isOpen)} aria-hidden="true">
        <Disclosure.Button
          className={`group w-full flex items-center text-left text-base font-medium rounded-md p-2  ${
            isOpen ? "text-black opacity-90" : "hover:text-gray-600"
          }`}
        >
          <div className="mr-1">{item.icon}</div>
          <span className={`flex-1 `}>{item.label}</span>
          <svg
            className={`ml-3 mt-1 flex-shrink-0 h-5 w-5 transform  transition-colors ease-in-out duration-150 ${
              isOpen
                ? "text-black opacity-90 rotate-90"
                : "text-gray-400 group-hover:text-gray-400"
            }
          `}
            viewBox="0 0 20 20"
            aria-hidden="true"
          >
            <path d="M6 6L14 10L6 14V6Z" fill="currentColor" />
          </svg>
        </Disclosure.Button>
      </div>

      {isOpen && (
        <Disclosure.Panel className="space-y-1" static>
          {item.children.map((subItem: any) => (
            <Link href={subItem.href} key={subItem.label}>
              <p className="hover:no-underline">
                <Disclosure.Button
                  className={`w-full flex items-center pl-5 py-2 rounded-md text-base text-gray-400 font-medium ${
                    path === subItem.href
                      ? "text-black opacity-90"
                      : "hover:text-gray-600"
                  }`}
                >
                  {subItem.label}
                </Disclosure.Button>
              </p>
            </Link>
          ))}
        </Disclosure.Panel>
      )}
    </Disclosure>
  );
};

export default GroupItem;
