import React from "react";
import SignHeader from "../Header/SignHeader";

export default function SignLayout({
  children,
}: {
  children: JSX.Element | JSX.Element[];
}) {
  return (
    <div className="overflow-hidden">
      <header className="bg-[#171717] min-h-[89px] flex justify-start items-center">
        <SignHeader />
      </header>
      <div className="h-screen w-screen flex justify-center items-center bg-black">
        {children}
      </div>
    </div>
  );
}
