import React from "react";
import { AiOutlineSearch } from "react-icons/ai";
import { FiUser, FiMenu } from "react-icons/fi";
import { BsCart2 } from "react-icons/bs";
import { useDispatch } from "react-redux";
import { openMenu } from "../../redux/authSlice";
import Link from "next/link";
import UseHref from "../MenuDrop/UseHref";

const NotLogger = () => {
  const optionUser = [
    {
      title: "Đăng Nhập",
      href: "/login",
    },
    {
      title: "Đăng Ký",
      href: "/register",
    },
    {
      title: "Đơn Hàng",
      href: "/",
    },
  ];

  const dispatch = useDispatch();
  return (
    <div className="cursor-pointer flex space-x-4 items-center text-2xl md:text-2xl">
      <AiOutlineSearch />
      <Link href="/cart">
        <div>
          <BsCart2 />
        </div>
      </Link>
      <FiMenu
        className="md:hidden"
        onClick={() => {
          dispatch(openMenu());
        }}
      />
      <UseHref className="w-44 -right-3" options={optionUser}>
        <FiUser className="hidden md:block" />
      </UseHref>
    </div>
  );
};

export default NotLogger;
