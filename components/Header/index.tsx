import React from "react";
import Logger from "./Logger";
import NotLogger from "./NotLogger";
import { useEffect, useState } from "react";
import Cookies from "js-cookie";
import NavBar from "../NavBar";
import Image from "next/image";

const Header = () => {
  const [users, setUsers] = useState<string>();

  const token = Cookies.get("token");

  useEffect(() => {
    if (token) {
      setUsers(token);
    }
  }, [token]);
  return (
    <div className="flex items-center justify-between h-14 md:h-20 md:max-w-screen-2xl mx-auto px-4 sm:px-6 lg:px-8">
      <Image src="/images/logo.svg" alt="" width={104} height={89} />
      <NavBar />
      <div>{users ? <Logger /> : <NotLogger />}</div>
    </div>
  );
};

export default Header;
