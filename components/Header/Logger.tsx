import React from "react";
import { AiOutlineSearch } from "react-icons/ai";
import { FiUser, FiMenu } from "react-icons/fi";
import { BsCart2 } from "react-icons/bs";
import { useDispatch } from "react-redux";
import { openMenu } from "../../redux/authSlice";
import Link from "next/link";
import UseHref from "../MenuDrop/UseHref";

const Logger = () => {
  const optionUser = [
    {
      title: "Đơn Hàng",
      href: "/",
    },
  ];

  const dispatch = useDispatch();
  return (
    <div className="cursor-pointer flex space-x-4 items-center text-2xl md:text-2xl">
      <AiOutlineSearch />
      <Link href="/cart">
        <BsCart2 />
      </Link>
      <FiMenu
        className="md:hidden"
        onClick={() => {
          dispatch(openMenu());
        }}
      />
      <UseHref className="w-52 -right-3" options={optionUser}>
        <div className="bg-gray-200 w-10 h-10 rounded-xl flex items-center justify-center">
          <FiUser className="hidden md:block" />
        </div>
      </UseHref>
    </div>
  );
};

export default Logger;
