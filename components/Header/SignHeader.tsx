import React from "react";
import Image from "next/image";

const SignHeader = () => {
  return (
    <div className="h-full max-w-7xl">
      <Image
        className="invert"
        src="/images/txt_logo.svg"
        alt=""
        width={200}
        height={89}
      />
    </div>
  );
};

export default SignHeader;
