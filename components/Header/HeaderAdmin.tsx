import React from "react";

const HeaderAdmin = () => {
  return (
    <div className="bg-gradient-to-tr from-cyan-500 via-sky-500 to-blue-500 w-full h-full"></div>
  );
};

export default HeaderAdmin;
