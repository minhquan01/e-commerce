import React from "react";
import { BsSearch } from "react-icons/bs";
import { AiFillCloseCircle } from "react-icons/ai";
import { useState } from "react";

const HelpSearch = () => {
  const [searchKey, setSearchKey] = useState("");
  return (
    <div className="mt-10 md:mt-0 mb-10">
      <h1 className="text-2xl px-10 font-semibold md:font-bold text-center mb-8 md:mt-20 md:text-4xl">
        Chúng tôi có thể giúp bạn tìm kiếm?
      </h1>
      <div className="flex items-center border border-black w-full md:w-1/3 bg-slate-100 mx-auto rounded-full">
        <BsSearch className="ml-5 text-xl md:text-3xl" />
        <input
          type="text"
          className="py-2 w-full ml-4 mr-10 bg-transparent text-sm md:text-lg text-black outline-none placeholder:text-black opacity-90"
          placeholder="Search Keyword"
          onChange={(e) => setSearchKey(e.target.value)}
        />
        {searchKey ? <AiFillCloseCircle className="mr-8 text-2xl" /> : <></>}
      </div>
    </div>
  );
};

export default HelpSearch;
