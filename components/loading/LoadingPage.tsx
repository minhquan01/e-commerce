const LoadingPage = () => {
  return (
    <div className="fixed inset-0 z-[9999] flex justify-center gap-5 items-center h-full bg-white">
      <span className="bg-blue-500 w-2 h-2 rounded-full animate-ping shadow-md"></span>
      <span
        className="bg-blue-500 w-2 h-2 rounded-full animate-ping shadow-md"
        style={{
          animationDelay: "150ms",
        }}
      ></span>
      <span
        className="bg-blue-500 w-2 h-2 rounded-full animate-ping shadow-md"
        style={{
          animationDelay: "300ms",
        }}
      ></span>
    </div>
  );
};

export default LoadingPage;
