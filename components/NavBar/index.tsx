import Link from "next/link";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { GrClose } from "react-icons/gr";
import { useDispatch, useSelector } from "react-redux";
import { closeMenu } from "../../redux/authSlice";

const NavBar = () => {
  const router = useRouter();
  const path = router.pathname;
  const [pathSelected, setPathSelected] = useState("/");
  const hiddenMenu = useSelector((state: any) => state.auth.menu.open);

  const dispatch = useDispatch();

  useEffect(() => {
    setPathSelected(path);
  }, [path]);

  const customerNav = [
    {
      url: "/",
      label: "Trang Chủ",
    },
    {
      url: "/product",
      label: "Sản Phẩm",
    },
  ];
  return (
    <div>
      <div className="hidden md:block">
        <nav className="px-2 space-x-4 flex items-center">
          {customerNav.map((item, index) => (
            <Link href={item.url} key={index}>
              <p
                key={index}
                className={` px-2 py-1 flex items-center text-base font-medium  ${
                  pathSelected === item.url
                    ? " text-blue-500 hover:text-blue-500 "
                    : "text-black hover:text-blue-400"
                }`}
              >
                {item.label}
              </p>
            </Link>
          ))}
        </nav>
      </div>
      <div
        className={`md:hidden fixed inset-0 z-[999] bg-white transition-all duration-300 ${
          hiddenMenu ? "translate-x-[0%]" : "translate-x-full"
        }`}
      >
        <div className="p-5 w-screen h-screen ">
          <div className="w-full flex justify-end items-center">
            <div className="p-1">
              <GrClose
                className="text-2xl "
                onClick={() => {
                  dispatch(closeMenu());
                }}
              />
            </div>
          </div>
          <nav className="mt-5">
            <div className="border-b-2 mb-5 pb-2">
              {customerNav.map((item, index) => (
                <Link href={item.url} key={index}>
                  <p
                    key={index}
                    onClick={() => {
                      dispatch(closeMenu());
                    }}
                    className={`px-2 py-1  flex items-center justify-center my-2 text-xl font-normal  ${
                      pathSelected === item.url
                        ? " bg-blue-500 rounded-full text-white"
                        : "text-black "
                    }`}
                  >
                    {item.label}
                  </p>
                </Link>
              ))}
            </div>
            <div className="flex flex-col space-y-4">
              <Link href="/login">
                <p className="border-2 flex items-center justify-center cursor-pointer text-lg py-2 border-blue-400 rounded-full">
                  Đăng nhập
                </p>
              </Link>
              <Link href="/register">
                <p className="border-2 flex items-center justify-center cursor-pointer text-lg py-2 border-blue-400 rounded-full">
                  Đăng ký
                </p>
              </Link>
            </div>
          </nav>
        </div>
      </div>
    </div>
  );
};

export default NavBar;
