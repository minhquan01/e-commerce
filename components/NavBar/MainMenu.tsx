import Image from "next/image";
import Link from "next/link";
import React from "react";
import { AiFillPieChart } from "react-icons/ai";
import { BsFillImageFill } from "react-icons/bs";
import { IoExit } from "react-icons/io5";
import { TbUsers, TbBuildingWarehouse } from "react-icons/tb";
import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import Cookies from "js-cookie";
import { decodeToken } from "react-jwt";
import GroupItem from "../Layout/GroupItem";

const adminMenu = [
  {
    href: "/dashboard",
    label: "Dashboard",
    icon: <AiFillPieChart />,
  },
  {
    href: "/account",
    label: "Account",
    icon: <TbUsers />,
  },
  {
    label: "Banner",
    icon: <BsFillImageFill />,
    children: [
      { href: "/banner-slider", label: "Slider" },
      { href: "/banner-featured", label: "Featured" },
      { href: "/banner-mobile", label: "Mobile" },
      { href: "/banner-tv", label: "Television" },
      { href: "/banner-houseware", label: "HouseWare" },
      { href: "/banner-explore", label: "Explore" },
    ],
  },
  {
    label: "Product",
    icon: <TbBuildingWarehouse />,
    href: "/product-management",
  },
];

const MainMenu = () => {
  const [pathSelected, setPathSelected] = useState("/dashboard");
  const [fullName, setFullName] = useState<string>("");
  const router = useRouter();
  const path = router.pathname;
  const token = Cookies.get("token");

  const handleSignOut = () => {
    router.push("/");
    Cookies.remove("token");
  };

  useEffect(() => {
    if (token) {
      const jwtToken: any = decodeToken(token);
      const fristName = jwtToken.firstName
        ?.substring(0, 1)
        .concat(
          jwtToken.firstName
            ?.substring(1, jwtToken.firstName.length)
            .toLowerCase()
        );
      const lastName = jwtToken.lastName
        ?.substring(0, 1)
        .concat(
          jwtToken.lastName
            ?.substring(1, jwtToken.lastName.length)
            .toLowerCase()
        );
      setFullName(fristName + " " + lastName);
    }
  }, [token]);

  useEffect(() => {
    setPathSelected(path);
  }, [path]);

  return (
    <div className="w-full h-full flex flex-col mt-5 py-3 items-center">
      <Image src="/images/logo.svg" alt="" width={104} height={89} />

      <div className="mt-4 flex flex-col items-center space-y-1">
        <Image
          src="/images/avatar.jpg"
          className="rounded-full"
          alt=""
          width={60}
          height={60}
        />
        <p className="text-sm opacity-75">Welcome back</p>
        <span className="font-semibold">{fullName}</span>
      </div>
      <div className="mt-5 flex flex-col items-center h-full justify-between">
        <div className="h-[394px] overflow-y-auto px-11">
          {adminMenu.map((item, index) =>
            item.href ? (
              <Link href={item.href} key={index}>
                <div
                  key={index}
                  className={`group hover:no-underline flex items-center px-2 py-2 text-base font-medium text-gray-400 ${
                    pathSelected === item.href
                      ? "text-black opacity-90"
                      : "hover:text-gray-600"
                  }`}
                >
                  <div className="mr-1">{item.icon}</div>
                  {item.label}
                </div>
              </Link>
            ) : (
              <GroupItem item={item} key={index} />
            )
          )}
        </div>
        <div
          onClick={handleSignOut}
          className="cursor-pointer flex items-center mb-5 space-x-1 py-2 text-base font-medium text-gray-700 hover:text-red-600"
        >
          <IoExit />
          <p>Sign out</p>
        </div>
      </div>
    </div>
  );
};

export default MainMenu;
