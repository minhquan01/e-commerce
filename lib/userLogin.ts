import { decodeToken } from "react-jwt";
import Cookies from "js-cookie";

export const userLogin = () => {
  const token = Cookies.get("token");
  if (token) {
    const user = decodeToken(token);
    return user;
  }
};
