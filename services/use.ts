import axios from "axios";
import { User, GetUsersQuery, Role, UpdateProfile } from "../interfaces/user";

export const registerUser = async (user: User) => {
  await axios.post(`/api/user/register`, {
    firstName: user.firstName,
    lastName: user.lastName,
    email: user.email,
    password: user.password,
  });
};

export const loginUser = async (user: User) => {
  return await axios.post("api/user/login", {
    email: user.email,
    password: user.password,
  });
};

export const logoutUser = async () => {
  await axios.post("/api/user/logout");
};

export const getAllUser = async (query: GetUsersQuery) => {
  return await axios.get("/api/user/account", { params: query });
};

export const updateAdmin = async (user: Role) => {
  return await axios.put("/api/user/account", {
    user,
  });
};

export const getProfileUser = async (id: string) => {
  return await axios.get("/api/user/profile", { params: { id } });
};

export const updateProfileUser = async (user: UpdateProfile) => {
  return await axios.put("/api/user/profile", { user });
};
