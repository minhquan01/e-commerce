import axios from "axios";
import {
  GetBannerQuery,
  UploadBannerMobile,
  UploadBannerTv,
} from "../interfaces/image";

export const uploadBannerSlider = async (url: string) => {
  return axios.post("api/image/banner-slider", { url });
};

export const getBannerSlider = async (query: GetBannerQuery) => {
  return axios.get("api/image/banner-slider", { params: query });
};

export const publishBannerSlider = async (selectId: string[]) => {
  return axios.put("api/image/publish", { selectId });
};

export const unPublishBannerSlider = async (selectId: string[]) => {
  return axios.put("api/image/un-publish", { selectId });
};

export const getBannerPublish = async () => {
  return axios.get("api/image/publish");
};

export const uploadBannerMobile = async (dataImage: UploadBannerMobile) => {
  return axios.post("api/image/banner-mobile", { dataImage });
};

export const getBannerMobile = async (query: GetBannerQuery) => {
  return axios.get("api/image/banner-mobile", { params: query });
};

export const deleteBannerMobile = async (selectId: string[]) => {
  return axios.put("api/image/banner-mobile", { selectId });
};

export const getAllBannerMobile = async () => {
  return axios.get("api/image/get-all/banner-mobile-all");
};

export const uploadBannerTv = async (dataImage: UploadBannerTv) => {
  return axios.post("api/image/banner-tv", { dataImage });
};

export const getBannerTelevision = async (query: GetBannerQuery) => {
  return axios.get("api/image/banner-tv", { params: query });
};

export const deleteBannerTv = async (selectId: string[]) => {
  return axios.put("api/image/banner-tv", { selectId });
};

export const getAllBannerTv = async () => {
  return axios.get("api/image/get-all/banner-tv-all");
};

export const uploadBannerHouseWare = async (dataImage: UploadBannerTv) => {
  return axios.post("api/image/banner-houseware", { dataImage });
};

export const getBannerHouseWare = async (query: GetBannerQuery) => {
  return axios.get("api/image/banner-houseware", { params: query });
};

export const deleteBannerHouseWare = async (selectId: string[]) => {
  return axios.put("api/image/banner-houseware", { selectId });
};

export const getAllBannerHouseWare = async () => {
  return axios.get("api/image/get-all/banner-houseware-all");
};

export const uploadBannerExplore = async (dataImage: UploadBannerTv) => {
  return axios.post("api/image/banner-explore", { dataImage });
};

export const getBannerExplore = async (query: GetBannerQuery) => {
  return axios.get("api/image/banner-explore", { params: query });
};

export const deleteBannerExplore = async (selectId: string[]) => {
  return axios.put("api/image/banner-explore", { selectId });
};

export const getAllBannerExplore = async () => {
  return axios.get("api/image/get-all/banner-explore-all");
};
