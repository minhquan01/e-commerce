
import axios from 'axios';
import { ProductUpload } from '../interfaces/product';
import { GetProductQuery } from './../interfaces/user.d';

export const getProducts = async (query: GetProductQuery) => {
  return await axios.get("/api/product", { params: query });
};

export const addProducts = async (product: ProductUpload) => {
  return await axios.post('/api/product', { product })
}

export const editProducts = async (product: ProductUpload) => {
  return await axios.put('/api/product', { product })
}

export const deleteProducts = async (id: string) => {
  return await axios.post('/api/product/delete', { id })
}