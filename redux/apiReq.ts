import { AnyAction } from "@reduxjs/toolkit";
import { NextRouter } from "next/router";
import { Dispatch } from "react";
import { User } from "../interfaces/user";
import { loginUser, registerUser } from "../services/use";
import {
  loginFailed,
  loginSuccess,
  registerFailed,
  registerSuccess,
} from "./authSlice";

export const register = async (
  user: User,
  dispatch: Dispatch<AnyAction>,
  router: NextRouter
) => {
  try {
    await registerUser(user);
    dispatch(registerSuccess());
    router.push("/login");
  } catch (err) {
    dispatch(registerFailed(err));
  }
};

export const login = async (
  user: User,
  dispatch: Dispatch<AnyAction>,
  router: NextRouter
) => {
  try {
    const res = await loginUser(user);
    dispatch(loginSuccess(res.data));
    if (res.data.admin) {
      router.push("/dashboard");
    } else {
      router.push("/");
    }
  } catch (err) {
    dispatch(loginFailed(err));
  }
};
