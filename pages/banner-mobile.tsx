import React, { useCallback } from "react";
import { ReactElement } from "react";
import ContentAdmin from "../components/Layout/ContentAdmin";
import MainAdmin from "../components/Layout/MainAdmin";
import {
  deleteBannerMobile,
  getBannerMobile,
  getBannerSlider,
  publishBannerSlider,
  unPublishBannerSlider,
} from "../services/image";
import { useState, useEffect } from "react";
import Card from "../components/Card";
import Pagination from "../components/Pagination";
import { useRouter } from "next/router";
import {
  BannerMobileProps,
  BannerSlider,
  GetBannerQuery,
} from "../interfaces/image";
import dateFormat from "dateformat";
import Modal from "../components/Modal";
import { AiOutlinePlus, AiOutlineDelete } from "react-icons/ai";
import { toast } from "react-toastify";
import LoadingPage from "../components/loading/LoadingPage";
import UploadBannnerMobile from "../containers/Upload/UploadBannerMobile";

const BannerMobile = () => {
  const DEFAULT_BANNERMOBILE_LIMIT = 6;
  const router = useRouter();
  const [dataImage, setDataImage] = useState([]);
  const [totalImage, setTotalImage] = useState(0);
  const [openModal, setOpenModal] = useState(false);
  const [selectedImage, setSelectedImage] = useState<any>([]);
  const [selectedId, setSelectedId] = useState<any>([]);
  const [loading, setLoading] = useState(true);

  const dataBannerMobile = async (query?: GetBannerQuery): Promise<void> => {
    try {
      const { data } = await getBannerMobile({
        ...query,
        limit: DEFAULT_BANNERMOBILE_LIMIT,
      });
      setDataImage(data.result);
      setTotalImage(data.total);
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  };

  const onChangePage = (page: number) => {
    router.push({
      query: {
        page: page,
      },
    });
  };

  useEffect(() => {
    const query = handleQueryParams(router.query);
    dataBannerMobile(query);
  }, [router.query, openModal]);

  const handleQueryParams = (query: any) => {
    const newQuery = query;

    if (!newQuery.page) {
      query.page = "1";
      router.push(
        {
          query: {
            ...newQuery,
          },
        },
        undefined,
        { shallow: true }
      );
    }
    return newQuery;
  };

  const handleSelectBanner = useCallback(
    (image: BannerMobileProps) => {
      if (selectedId.length !== 0 && selectedImage.includes(image)) {
        const res = selectedImage.filter(
          (item: BannerMobileProps) => item !== image
        );
        setSelectedImage(res);
        const resId = selectedId.filter((item: string) => item !== image.id);
        setSelectedId(resId);
      } else {
        setSelectedImage([...selectedImage, image]);
        setSelectedId([...selectedId, image.id]);
      }
    },
    [selectedImage, selectedId]
  );

  const checkImageSelected = useCallback(
    (image: BannerMobileProps) => {
      return selectedImage.some((item: BannerMobileProps) => {
        return item.id === image.id;
      });
    },
    [selectedImage]
  );

  const handleDeleteImage = async () => {
    try {
      const res = await deleteBannerMobile(selectedId);
      if (res.status === 200) {
        toast.success("Đã Xóa");
      }
      dataBannerMobile();
      setSelectedId([]);
      setSelectedImage([]);
    } catch (error) {
      toast.error("Có lỗi gì đó");
    }
  };

  return (
    <>
      {loading && <LoadingPage />}
      <ContentAdmin title="Banner Mobile">
        <div className="flex justify-between items-center mb-5">
          <button
            onClick={handleDeleteImage}
            className="flex items-center space-x-1 px-3  py-1 rounded-lg  font-semibold bg-blue-500 hover:bg-blue-600 text-white"
          >
            <AiOutlineDelete />
            <p>Xóa ảnh</p>
          </button>
          <button
            onClick={() => setOpenModal(true)}
            className="flex items-center space-x-1 px-3  py-1 rounded-lg  font-semibold bg-blue-500 hover:bg-blue-600 text-white"
          >
            <AiOutlinePlus />
            <p>Thêm ảnh</p>
          </button>
        </div>
        <Card>
          <Card.Content>
            <div className="grid grid-cols-3 gap-4">
              {dataImage.map((item: BannerMobileProps, idx) => {
                return (
                  <div
                    onClick={() => handleSelectBanner(item)}
                    className={`relative flex rounded-md cursor-pointer p-1 flex-col items-center space-y-1 ${
                      checkImageSelected(item) ? "border-4 border-blue-500" : ""
                    }`}
                    key={idx}
                  >
                    <img
                      src={item.url}
                      className="rounded-md w-full h-[175px] object-cover"
                      alt=""
                    />
                    <div className="flex flex-col items-center">
                      <h4 className="font-semibold">{item.name}</h4>
                      <p className="text-sm">{item.description}</p>
                      <p className="text-sm">
                        {dateFormat(item.createdAt, "dd/mm/yyyy")}
                      </p>
                    </div>
                  </div>
                );
              })}
            </div>
          </Card.Content>
          <Pagination
            current={Number(router.query.page || 1)}
            pageSize={DEFAULT_BANNERMOBILE_LIMIT}
            total={totalImage}
            onChange={onChangePage}
          />
        </Card>
      </ContentAdmin>
      <Modal title="Thêm hình ảnh" open={openModal} setOpen={setOpenModal}>
        <UploadBannnerMobile open={openModal} setOpen={setOpenModal} />
      </Modal>
    </>
  );
};

BannerMobile.getLayout = function getLayout(page: ReactElement) {
  return <MainAdmin>{page}</MainAdmin>;
};

export default BannerMobile;
