import React, { ReactElement, useEffect, useMemo, useState } from "react";
import { CustomHeader } from "../components/Header/CustomHeader";
import MainAdmin from "../components/Layout/MainAdmin";
import Table from "./../components/Table";
import { useRouter } from "next/router";
import { GetUsersQuery } from "../interfaces/user";
import { deleteProducts, getProducts } from "./../services/product";
import Card from "../components/Card";
import Pagination from "./../components/Pagination/index";
import { AiOutlinePlus } from "react-icons/ai";
import Modal from "../components/Modal";
import UploadProduct from "./../containers/Upload/UploadProduct";
import { ListProduct } from "./../interfaces/product.d";
import ModalImg from "./../components/Modal/ModalImg";
import { toast } from "react-toastify";

const DEFAULT_PRODUCTS_LIMIT = 6;

const ProductManagement = () => {
  const column = [
    "Thứ tự",
    "Tên sản phẩm",
    "Giá sản phẩm",
    "Ảnh nổi bật",
    "Bộ nhớ",
    "Màn hình",
    "Camera",
    "Màu",
    "Ảnh chi tiết",
    "",
    "",
  ];
  const [loading, setLoading] = useState(true);
  const [products, setProducts] = useState([]);
  const [dataEdit, setDataEdit] = useState<ListProduct>();
  const [totalProducts, setTotalProducts] = useState(0);
  const [openModal, setOpenModal] = useState(false);
  const [add, setAdd] = useState(false);
  const [openImg, setOpenImg] = useState(false);
  const [imgModal, setImgModal] = useState("");

  const router = useRouter();
  let count = DEFAULT_PRODUCTS_LIMIT * (Number(router.query.page) - 1) + 1;

  const fetchUsers = async (query?: GetUsersQuery): Promise<void> => {
    try {
      const { data } = await getProducts({
        ...query,
        limit: DEFAULT_PRODUCTS_LIMIT,
      });

      setProducts(data.products);
      setTotalProducts(data.total);
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  };

  const onChangePage = (page: number) => {
    router.push({
      query: {
        page: page,
      },
    });
  };

  const handleClickBtnAdd = () => {
    setOpenModal(true);
    setAdd(true);
  };

  const handleModalImgMain = (url: string) => {
    setImgModal(url);
    setOpenImg(true);
  };

  const handleModalImgDetail = (image: string) => {
    setImgModal(image);
    setOpenImg(true);
  };

  const handleEditProd = (item: ListProduct) => {
    setDataEdit(item);
    setOpenModal(true);
  };

  const handleDeleteProd = async (id: string) => {
    const res = await deleteProducts(id);
    fetchUsers();
    if (res.status === 200) {
      toast.success("Đã xóa thành công sản phẩm!");
    }
  };

  const dataSource = useMemo(() => {
    return products.map((item: ListProduct, index: number) => {
      return [
        <> {count === 0 ? index + 1 : count++}</>,
        item.productDetail.name,
        Number(item.productDetail.price).toLocaleString("it-IT", {
          style: "currency",
          currency: "VND",
        }),
        <img
          className="rounded-md h-10 w-8 object-cover cursor-pointer"
          src={item.url}
          onClick={() => handleModalImgMain(item.url)}
        />,
        item.productDetail.memory.map((memo, idx) => (
          <div key={idx}>
            <p className="border-r border-black pr-1 last:border-none">
              {memo}
            </p>
          </div>
        )),
        item.productDetail.screen,
        item.productDetail.camera,
        item.productDetail.color.map((col, idx) => (
          <p className="border-r border-black pr-1 last:border-none" key={idx}>
            {col}
          </p>
        )),
        item.productDetail.imageDetail.map((image, idx) => (
          <img
            key={idx}
            src={image}
            onClick={() => handleModalImgDetail(image)}
            className="rounded-md h-10 w-8 object-cover cursor-pointer"
          />
        )),
        <p
          onClick={() => handleEditProd(item)}
          className="cursor-pointer text-blue-300 hover:text-blue-500"
        >
          Sửa
        </p>,
        <p
          onClick={() => handleDeleteProd(item.id)}
          className="cursor-pointer text-red-300 hover:text-red-500"
        >
          Xóa
        </p>,
      ];
    });
  }, [products]);

  useEffect(() => {
    const query = handleQueryParams(router.query);
    fetchUsers();
  }, [router.query, openModal]);

  const handleQueryParams = (query: any) => {
    const newQuery = query;

    if (!newQuery.page) {
      query.page = "1";
      router.push(
        {
          query: {
            ...newQuery,
          },
        },
        undefined,
        { shallow: true }
      );
    }
    return newQuery;
  };

  return (
    <>
      <CustomHeader title="Quản lý sản phẩm">
        <title>Product Management</title>
      </CustomHeader>
      <div className="flex justify-end">
        <button
          onClick={handleClickBtnAdd}
          className="flex items-center mb-2 space-x-1 px-3  py-1 rounded-lg  font-semibold bg-blue-500 hover:bg-blue-600 text-white"
        >
          <AiOutlinePlus />
          <p>Thêm sản phẩm</p>
        </button>
      </div>
      <ModalImg open={openImg} setOpen={setOpenImg}>
        <img
          className="w-60 h-72 object-cover rounded-sm "
          src={imgModal}
          alt=""
        />
      </ModalImg>
      <Modal title="Thêm sản phẩm" open={openModal} setOpen={setOpenModal}>
        <UploadProduct
          dataEdit={dataEdit}
          open={openModal}
          openAdd={add}
          setAdd={setAdd}
          setOpen={setOpenModal}
        />
      </Modal>
      <Card>
        <Card.Content>
          <Table columns={column} dataSource={dataSource} loading={loading} />
        </Card.Content>
        <Pagination
          current={Number(router.query.page || 1)}
          pageSize={DEFAULT_PRODUCTS_LIMIT}
          total={totalProducts}
          onChange={onChangePage}
        />
      </Card>
    </>
  );
};

ProductManagement.getLayout = function getLayout(page: ReactElement) {
  return <MainAdmin>{page}</MainAdmin>;
};

export default ProductManagement;
