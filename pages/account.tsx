import React from "react";
import { ReactElement } from "react";
import MainAdmin from "../components/Layout/MainAdmin";
import User from "../containers/User";

const Account = () => {
  return (
    <>
      <User />
    </>
  );
};

Account.getLayout = function getLayout(page: ReactElement) {
  return <MainAdmin>{page}</MainAdmin>;
};

export default Account;
