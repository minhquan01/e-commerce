import React, { ReactElement, useEffect } from "react";
import { CustomHeader } from "../../components/Header/CustomHeader";
import LayoutNotFooter from "../../components/Layout/LayoutNotFooter";
import { userLogin } from "../../lib/userLogin";
import { getProfileUser, updateProfileUser } from "../../services/use";
import { useState } from "react";
import { toast } from "react-toastify";

const Profile = () => {
  const user: any = userLogin();
  const [textBtn, setTextBtn] = useState(false);
  const [valueFirstName, setValueFirstName] = useState("");
  const [valueLastName, setValueLastName] = useState("");
  const [valueEmail, setValueEmail] = useState("");
  const [valuePhone, setValuePhone] = useState("");
  const [valueAddress, setValueAddress] = useState("");

  const getProfile = async () => {
    try {
      const { data } = await getProfileUser(String(user.id));
      setValueFirstName(data.firstName);
      setValueLastName(data.lastName);
      setValueEmail(data.email);
      setValuePhone(data.phoneNumber ? data.phoneNumber : "");
      setValueAddress(data.address ? data.address : "");
    } catch (error) {
      console.log(error);
    }
  };

  const handleUpdateProfile = async () => {
    const profileUpdate = {
      id: user.id,
      firstName: valueFirstName,
      lastName: valueLastName,
      phoneNumber: valuePhone,
      address: valueAddress,
    };
    const res = await updateProfileUser(profileUpdate);
    if (res.status === 200) {
      toast.success("Cập nhật thành công thông tin!");
    }
  };

  useEffect(() => {
    getProfile();
  }, []);

  return (
    <>
      <CustomHeader>
        <title>Tài khoản của tôi</title>
      </CustomHeader>
      <div className="w-[70%] mt-10 px-10 py-5 rounded-xl shadow-md mx-auto border">
        <div className="flex pb-5 items-center justify-between border-b mb-6">
          <div className=" text-xl font-bold">Thông tin cá nhân</div>

          <button
            onClick={() => setTextBtn(!textBtn)}
            className="border-2 border-blue-500 rounded-2xl font-semibold text-base px-5 py-1 hover:bg-blue-500 hover:text-white transition-all "
          >
            {textBtn ? (
              <div onClick={handleUpdateProfile}>
                <p>Lưu</p>
              </div>
            ) : (
              "Thay đổi"
            )}
          </button>
        </div>
        <dl className="divide-y divide-gray-200">
          <div className="py-4 sm:grid sm:grid-cols-2 sm:gap-4 sm:py-5">
            <dt className="text-sm font-medium text-gray-500">Họ</dt>
            <dd className="mt-1 flex text-sm text-gray-900 sm:col-span-1 sm:mt-0">
              {textBtn ? (
                <div className="border w-5/6 border-black rounded-lg py-1 pl-1">
                  <input
                    value={valueFirstName}
                    type="text"
                    className="w-full caret-blue-300 outline-none"
                    onChange={(e) => setValueFirstName(e.target.value)}
                  />
                </div>
              ) : (
                <span className="flex-grow">{valueFirstName}</span>
              )}
            </dd>
          </div>
          <div className="py-4 sm:grid sm:grid-cols-2 sm:gap-4 sm:py-5">
            <dt className="text-sm font-medium text-gray-500">Tên</dt>
            <dd className="mt-1 flex text-sm text-gray-900 sm:col-span-1 sm:mt-0">
              {textBtn ? (
                <div className="border w-5/6 border-black rounded-lg py-1 pl-1">
                  <input
                    value={valueLastName}
                    type="text"
                    className="w-full caret-blue-300 outline-none"
                    onChange={(e) => setValueLastName(e.target.value)}
                  />
                </div>
              ) : (
                <span className="flex-grow">{valueLastName}</span>
              )}
            </dd>
          </div>
          <div className="py-4 sm:grid sm:grid-cols-2 sm:gap-4 sm:py-5">
            <dt className="text-sm font-medium text-gray-500">Email</dt>
            <dd className="mt-1 flex text-sm text-gray-900 sm:col-span-1 sm:mt-0">
              {textBtn ? (
                <div className="flex flex-col space-y-1">
                  <span className="flex-grow">{valueEmail}</span>
                  <i className="text-xs text-red-400">
                    Không thể thay đổi email
                  </i>
                </div>
              ) : (
                <span className="flex-grow">{valueEmail}</span>
              )}
            </dd>
          </div>
          <div className="py-4 sm:grid sm:grid-cols-2 sm:gap-4 sm:py-5">
            <dt className="text-sm font-medium text-gray-500">Số điện thoại</dt>
            <dd className="mt-1 flex text-sm text-gray-900 sm:col-span-1 sm:mt-0">
              {textBtn ? (
                <div className="border w-5/6 border-black rounded-lg py-1 pl-1">
                  <input
                    value={valuePhone}
                    type="text"
                    className="w-full caret-blue-300 outline-none"
                    onChange={(e) => setValuePhone(e.target.value)}
                  />
                </div>
              ) : (
                <span className="flex-grow">{valuePhone}</span>
              )}
            </dd>
          </div>
          <div className="py-4 sm:grid sm:grid-cols-2 sm:gap-4 sm:py-5">
            <dt className="text-sm font-medium text-gray-500">Địa chỉ</dt>
            <dd className="mt-1 flex text-sm text-gray-900 sm:col-span-1 sm:mt-0">
              {textBtn ? (
                <div className="border w-5/6 border-black rounded-lg py-1 pl-1">
                  <input
                    value={valueAddress}
                    type="text"
                    className="w-full caret-blue-300 outline-none"
                    onChange={(e) => setValueAddress(e.target.value)}
                  />
                </div>
              ) : (
                <span className="flex-grow">{valueAddress}</span>
              )}
            </dd>
          </div>
        </dl>
      </div>
    </>
  );
};

Profile.getLayout = function getLayout(page: ReactElement) {
  return <LayoutNotFooter>{page}</LayoutNotFooter>;
};

export default Profile;
