import React, { ReactElement } from "react";
import SignUp from "../containers/SignUp";
import SignLayout from "../components/Layout/SignLayout";

const Register = () => {
  return (
    <>
      <SignUp />
    </>
  );
};

Register.getLayout = function getLayout(page: ReactElement) {
  return <SignLayout>{page}</SignLayout>;
};

export default Register;
