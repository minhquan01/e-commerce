import { ReactElement } from "react";
import { CustomHeader } from "../components/Header/CustomHeader";
import Mainlayout from "../components/Layout/MainLayout";
import Featured from "../components/FeaturedContent/Featured";
import SliderHome from "../containers/SliderHome";
import FeaturedImg from "../components/FeaturedContent/FeaturedImg";
import Explore from "../components/Explore";
import HelpSearch from "../components/HelpSearch";
import { GetBannerQuery } from "../interfaces/image";
import {
  getAllBannerMobile,
  getAllBannerHouseWare,
  getAllBannerExplore,
} from "../services/image";
import { useState, useEffect } from "react";
import FeaturedMobile from "../components/FeaturedContent/FeaturedMobile";

const products = [
  {
    name: "Ưu đãi",
    images: [
      "/images/products/uudai.jpg",
      "/images/products/uudai1.jpg",
      "/images/products/uudai2.jpg",
      "/images/products/uudai3.jpg",
      "/images/products/uudai4.jpg",
    ],
  },
  {
    name: "Điện thoại",
    images: ["/images/products/uudai.jpg"],
  },
  {
    name: "TV",
    images: ["/images/products/uudai.jpg"],
  },
  {
    name: "Gia dụng",
    images: ["/images/products/uudai.jpg"],
  },
  {
    name: "Màn hình",
    images: ["/images/products/uudai.jpg"],
  },
  {
    name: "Ưu đãi sinh viên",
    images: ["/images/products/uudai.jpg"],
  },
];
const phones = [
  {
    name: "Galaxy Z Flip4 Bespoke Edition ",
    images: ["/images/phones/phone1.jpg"],
    titleImg: "Galaxy Z Flip4 Bespoke Edition",
    descImg: "Độc quyền tại sam sung",
  },
  {
    name: " Galaxy Z Fold4 1TB",
    images: ["/images/phones/phone2.jpg"],
    titleImg: "Galaxy Z Flip4 Bespoke Edition2",
    descImg: "Độc quyền tại sam sung2",
  },
  {
    name: "Galaxy Z Flip4",
    images: ["/images/phones/phone3.jpg"],
    titleImg: "Galaxy Z Flip4 Bespoke Edition3",
    descImg: "Độc quyền tại sam sung3",
  },
  {
    name: " Galaxy Watch5 | Watch5 Pro",
    images: ["/images/phones/phone4.jpg"],
    titleImg: "Galaxy Z Flip4 Bespoke Edition4",
    descImg: "Độc quyền tại sam sung4",
  },
  {
    name: " Galaxy Buds2 Pro",
    images: ["/images/phones/phone5.jpg"],
    titleImg: "Galaxy Z Flip4 Bespoke Edition5",
    descImg: "Độc quyền tại sam sung5",
  },
];

const tvs = [
  {
    name: "Neo QLED 8K",
    images: ["/images/tvs/tv1.jpg"],
    titleImg: "Galaxy Z Flip4 Bespoke Edition",
  },
  {
    name: "QLED TV",
    images: ["/images/tvs/tv2.jpg"],
    titleImg: "Galaxy Z Flip4 Bespoke Edition2",
  },
  {
    name: " Crystal UHD 4K",
    images: ["/images/tvs/tv4.jpg"],
    titleImg: "Galaxy Z Flip4 Bespoke Edition4",
  },
];

const appliances = [
  {
    name: "BESPOKE",
    images: ["/images/appliances/gd1.jpg"],
    titleImg: "Galaxy Z Flip4 Bespoke Edition",
  },
  {
    name: "Tủ Lạnh Family Hub",
    images: ["/images/appliances/gd2.jpg"],
    titleImg: "Galaxy Z Flip4 Bespoke Edition2",
  },
  {
    name: "Máy Giặt Cửa Trước",
    images: ["/images/appliances/gd3.jpg"],
    titleImg: "Galaxy Z Flip4 Bespoke Edition4",
  },
  {
    name: "Tủ Chăm Sóc Quần Áo",
    images: ["/images/appliances/gd4.jpg"],
    titleImg: "Galaxy Z Flip4 Bespoke Edition4",
  },
  {
    name: "Máy Lọc Không Khí",
    images: ["/images/appliances/gd5.jpg"],
    titleImg: "Galaxy Z Flip4 Bespoke Edition4",
  },
];

const Home = () => {
  const [dataMobile, setDataMobile] = useState([]);
  const [dataTelevision, setDataTelevision] = useState([]);
  const [dataHouseWare, setDataHouseWare] = useState([]);
  const [dataExplore, setDataExplore] = useState([]);

  const dataBannerMobile = async (): Promise<void> => {
    try {
      const { data } = await getAllBannerMobile();
      setDataMobile(data.result);
    } catch (error) {
      console.log(error);
    } finally {
      // setLoading(false);
    }
  };
  const dataBannerTelevision = async (): Promise<void> => {
    try {
      const { data } = await getAllBannerMobile();
      setDataTelevision(data.result);
    } catch (error) {
      console.log(error);
    } finally {
      // setLoading(false);
    }
  };

  const dataBannerHouseWare = async (): Promise<void> => {
    try {
      const { data } = await getAllBannerHouseWare();
      setDataHouseWare(data.result);
    } catch (error) {
      console.log(error);
    } finally {
      // setLoading(false);
    }
  };

  const dataBannerExplore = async (): Promise<void> => {
    try {
      const { data } = await getAllBannerExplore();
      setDataExplore(data.result);
    } catch (error) {
      console.log(error);
    } finally {
      // setLoading(false);
    }
  };

  useEffect(() => {
    dataBannerMobile();
    dataBannerTelevision();
    dataBannerHouseWare();
    dataBannerExplore();
  }, []);

  return (
    <>
      <CustomHeader>
        <title>
          Samsung Việt Nam | Thiết Bị Di Động | Tivi | Điện Gia Dụng
        </title>
      </CustomHeader>
      <div>
        <SliderHome />
        <Featured title="Sản Phẩm Nổi Bật" categorys={products} />
        <FeaturedMobile title="Di Động" categorys={dataMobile} />
        <FeaturedImg
          title="TV & Loa"
          color="white"
          titleBtn="Mua ngay"
          categorys={dataTelevision}
        />
        <FeaturedImg
          title="Gia Dụng"
          color="black"
          titleBtn="Xem tất cả sản phẩm"
          categorys={dataHouseWare}
        />
        <Explore explores={dataExplore} />
        <HelpSearch />
      </div>
    </>
  );
};

Home.getLayout = function getLayout(page: ReactElement) {
  return <Mainlayout>{page}</Mainlayout>;
};

export default Home;
