import type { NextApiRequest, NextApiResponse } from "next";
import prisma from "../../../lib/prisma";
import bcrypt from "bcrypt";
import { User } from "../../../interfaces/user";

type Data = {};

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  const body = req.body;

  if (req.method === "POST") {
    createUser(body, res);
  }
}

async function createUser(body: User, res: NextApiResponse) {
  const salt = await bcrypt.genSalt(10);
  try {
    if (!body) {
      return res.status(400).json("not eixts");
    }

    const users = await prisma.user.findFirst({
      where: {
        email: body.email,
      },
    });

    if (users) {
      return res.status(400).json("Email already exists");
    }

    const hashed = await bcrypt.hash(body.password, salt);

    await prisma.user.create({
      data: {
        email: body.email,
        password: hashed,
        firstName: body.firstName,
        lastName: body.lastName,
      },
    });

    res.status(200).json("register successfully!");
  } catch (err) {
    console.log(err);

    res.status(400).json(err);
  }
}
