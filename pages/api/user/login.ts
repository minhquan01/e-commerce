import type { NextApiRequest, NextApiResponse } from "next";
import { User } from "../../../interfaces/user";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import prisma from "../../../lib/prisma";
import { serialize } from "cookie";

export default function handlerLogin(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const body = req.body;

  Login(body, res);
}

async function Login(body: User, res: NextApiResponse) {
  const secret = process.env.SECRET!;

  try {
    if (!body) {
      return res.status(400).json("not eixts");
    }

    const user = await prisma.user.findFirst({
      where: {
        email: body.email,
      },
    });

    if (!user) {
      return res.status(400).json("Incorrect email");
    }

    const validPassword = await bcrypt.compare(
      body.password,
      String(user?.password)
    );

    if (!validPassword) {
      return res.status(400).json("Incorrect password");
    }

    const token = jwt.sign(
      {
        id: user.id,
        email: user.email,
        firstName: user.firstName,
        lastName: user.lastName,
        admin: user.admin,
      },
      secret
    );
    res.setHeader("Set-Cookie", serialize("token", token, { path: "/" }));

    return res.status(200).json({
      id: user.id,
      email: user.email,
      name: user.lastName,
      admin: user.admin,
    });
  } catch (error) {
    res.status(400).json(error);
  }
}
