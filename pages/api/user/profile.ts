import type { NextApiRequest, NextApiResponse } from "next";
import { UpdateProfile } from "../../../interfaces/user";
import prisma from "../../../lib/prisma";

export default async function profile(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method === "GET") {
    const id = req.query.id;

    if (id) {
      await getProfile(res, String(id));
    } else {
      res.status(500).json("Not data");
    }
  }
  if (req.method === "PUT") {
    const userUpdate = req.body.user;

    await updateProfile(res, userUpdate);
  }

  async function getProfile(res: NextApiResponse, id: string) {
    try {
      const profile = await prisma.user.findFirst({
        where: {
          id: id,
        },
      });
      res.status(200).json(profile);
    } catch (error) {
      res.status(500).json(error);
    }
  }

  async function updateProfile(
    res: NextApiResponse,
    userUpdate: UpdateProfile
  ) {
    try {
      await prisma.user.update({
        where: {
          id: String(userUpdate.id),
        },
        data: {
          firstName: userUpdate.firstName,
          lastName: userUpdate.lastName,
          phoneNumber: userUpdate.phoneNumber,
          address: userUpdate.address,
        },
      });
      res.status(200).json("Update Success!");
    } catch (error) {
      res.status(500).json(error);
    }
  }
}
