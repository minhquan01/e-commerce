import type { NextApiRequest, NextApiResponse } from "next";
import { ProductUpload } from "../../../interfaces/product";
import { GetProductQuery } from './../../../interfaces/user.d';
import prisma from './../../../lib/prisma';

export default async function product(
    req: NextApiRequest,
    res: NextApiResponse
) {
    if (req.method === 'GET') {
        const query = req.query
        await getProduct(res, query)
    }
    if (req.method === 'POST') {
        const products = req.body.product
        if (products) {
            await addProduct(products, res)
        }
    }
    if (req.method === "PUT") {
        const product = req.body.product
        if (product) {
            await editProduct(product, res)
        }
    }

    async function getProduct(res: NextApiResponse, query: GetProductQuery) {
        try {
            const result = await prisma.product.findMany({
                skip: (Number(query.page || 1) - 1) * 6,
                take: Number(query.limit),
                include: {
                    productDetail: true

                }
            })

            const total = await prisma.product.count()

            res.status(200).json({ products: result, total })
        } catch (error) {
            res.status(500).json(error)
        }
    }

    async function addProduct(products: ProductUpload, res: NextApiResponse) {
        try {
            const result = await prisma.product.create({
                data: {
                    url: products.urlProd,
                    productDetail: {
                        create: {
                            name: products.nameProd,
                            camera: products.cameraProd,
                            memory: products.memoryProd,
                            price: products.priceProd,
                            screen: products.screenProd,
                            color: products.listColor,
                            imageDetail: products.listImage
                        }
                    }
                }
            })
            res.status(200).json(result)
        } catch (error) {
            res.status(500).json(error)
        }

    }

    async function editProduct(product: ProductUpload, res: NextApiResponse) {
        try {
            await prisma.product.update({
                where: {
                    id: product.idProdEdit
                },
                data: {
                    url: product.urlProd
                }
            })
            await prisma.productDetail.update({
                where: {
                    idProduct: product.idProdEdit
                },
                data: {
                    name: product.nameProd,
                    camera: product.cameraProd,
                    memory: product.memoryProd,
                    price: product.priceProd,
                    screen: product.screenProd,
                    color: product.listColor,
                    imageDetail: product.listImage
                }
            })
            res.status(200).json('Success')
        } catch (error) {
            res.status(500).json(error)
        }
    }
}