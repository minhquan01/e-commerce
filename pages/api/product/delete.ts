import type { NextApiRequest, NextApiResponse } from "next";
import prisma from './../../../lib/prisma';

export default async function product(
    req: NextApiRequest,
    res: NextApiResponse
) {
    if (req.method === 'POST') {
        const id = req.body.id
        if (id) {
            await deleteProd(id, res)
        }
    }

    async function deleteProd(id: string, res: NextApiResponse) {
        try {
            await prisma.productDetail.delete({
                where: {
                    idProduct: id
                }
            })
            await prisma.product.delete({
                where: {
                    id: id
                }
            })
            res.status(200).json('Success')
        } catch (error) {
            res.status(500).json(error)
        }
    }
}