import { NextApiRequest, NextApiResponse } from "next";
import prisma from "../../../lib/prisma";

export default function handleUnPushlish(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method === "PUT") {
    const selectId = req.body.selectId;

    unPublishImage(res, selectId);
  }
}

async function unPublishImage(res: NextApiResponse, selectId: string[]) {
  try {
    await prisma.bannerSlider.updateMany({
      where: {
        id: { in: selectId },
      },
      data: {
        publish: false,
      },
    });

    res.status(200).json("Success");
  } catch (error) {
    res.status(500).json(error);
  }
}
