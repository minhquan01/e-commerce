import { NextApiRequest, NextApiResponse } from "next";
import { GetBannerQuery, UploadBannerMobile } from "../../../interfaces/image";
import prisma from "../../../lib/prisma";

export default function handleBannerHouseWare(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method === "POST") {
    const dataImage = req.body.dataImage;
    saveImage(res, dataImage);
  }
  if (req.method === "GET") {
    const query = req.query;
    getImage(res, query);
  }
  if (req.method === "PUT") {
    const selectId = req.body.selectId;

    deleteImage(res, selectId);
  }
}

async function saveImage(res: NextApiResponse, dataImage: UploadBannerMobile) {
  try {
    await prisma.bannerHouseWare.create({
      data: {
        url: dataImage.url,
        name: dataImage.name,
      },
    });
    res.status(200).json("Upload Success!");
  } catch (error) {
    res.status(500).json("Upload Failed!");
  }
}

async function getImage(res: NextApiResponse, query: GetBannerQuery) {
  try {
    const result = await prisma.bannerHouseWare.findMany({
      skip: (Number(query.page || 1) - 1) * 6,
      take: Number(query.limit),
    });
    const total = await prisma.bannerHouseWare.count();
    res.status(200).json({ result, total });
  } catch (error) {
    res.status(500).json("Not found data!");
  }
}

async function deleteImage(res: NextApiResponse, selectId: string[]) {
  try {
    await prisma.bannerHouseWare.deleteMany({
      where: {
        id: { in: selectId },
      },
    });

    res.status(200).json("Success");
  } catch (error) {
    res.status(500).json(error);
  }
}
