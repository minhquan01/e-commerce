import { NextApiRequest, NextApiResponse } from "next";
import prisma from "../../../lib/prisma";

export default function handlePushlish(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method === "PUT") {
    const selectId = req.body.selectId;

    publishImage(res, selectId);
  }
  if (req.method === "GET") {
    getPublishBanner(res);
  }
}

async function publishImage(res: NextApiResponse, selectId: string[]) {
  try {
    await prisma.bannerSlider.updateMany({
      where: {
        id: { in: selectId },
      },
      data: {
        publish: true,
      },
    });

    res.status(200).json("Success");
  } catch (error) {
    res.status(500).json(error);
  }
}

async function getPublishBanner(res: NextApiResponse) {
  try {
    const result = await prisma.bannerSlider.findMany({
      where: {
        publish: true,
      },
    });
    res.status(200).json(result);
  } catch (error) {
    res.status(500).json(error);
  }
}
