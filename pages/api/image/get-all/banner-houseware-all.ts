import { NextApiRequest, NextApiResponse } from "next";
import prisma from "../../../../lib/prisma";

export default function handleBannerHouseWare(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method === "GET") {
    getImage(res);
  }
}

async function getImage(res: NextApiResponse) {
  try {
    const result = await prisma.bannerTelevision.findMany();
    res.status(200).json({ result });
  } catch (error) {
    res.status(500).json("Not found data!");
  }
}
