import { NextApiRequest, NextApiResponse } from "next";
import prisma from "../../../lib/prisma";
import { GetBannerQuery } from "../../../interfaces/image";

export default function handleBannerSlider(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method === "POST") {
    const url = req.body.url;
    saveImage(res, url);
  }
  if (req.method === "GET") {
    const query = req.query;
    getImage(res, query);
  }
}

async function saveImage(res: NextApiResponse, url: string) {
  try {
    await prisma.bannerSlider.create({
      data: {
        url: String(url),
      },
    });
    res.status(200).json("Upload Success!");
  } catch (error) {
    res.status(500).json("Upload Failed!");
  }
}

async function getImage(res: NextApiResponse, query: GetBannerQuery) {
  try {
    const result = await prisma.bannerSlider.findMany({
      skip: (Number(query.page || 1) - 1) * 6,
      take: Number(query.limit),
    });
    const total = await prisma.bannerSlider.count();
    res.status(200).json({ result, total });
  } catch (error) {
    res.status(500).json("Not found data!");
  }
}
