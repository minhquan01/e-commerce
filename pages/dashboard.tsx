import React from "react";
import { ReactElement } from "react";
import MainAdmin from "../components/Layout/MainAdmin";
import ContentAdmin from "../components/Layout/ContentAdmin";

const Dashboard = () => {
  return (
    <>
      <div>
        <ContentAdmin title="Dashboard">alo</ContentAdmin>
      </div>
    </>
  );
};
Dashboard.getLayout = function getLayout(page: ReactElement) {
  return <MainAdmin>{page}</MainAdmin>;
};

export default Dashboard;
