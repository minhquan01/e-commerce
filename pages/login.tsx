import React, { ReactElement } from "react";
import SignLayout from "../components/Layout/SignLayout";
import SignIn from "../containers/SignIn";

const Login = () => {
  return (
    <>
      <SignIn />
    </>
  );
};

Login.getLayout = function getLayout(page: ReactElement) {
  return <SignLayout>{page}</SignLayout>;
};

export default Login;
