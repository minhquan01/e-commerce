import React from "react";
import Mainlayout from "../../components/Layout/MainLayout";
import { ReactElement, Fragment } from "react";
import { CustomHeader } from "../../components/Header/CustomHeader";
import { Dialog, Disclosure, Transition } from "@headlessui/react";
import { IoIosArrowDown, IoIosArrowUp } from "react-icons/io";
import MenuDrop from "../../components/MenuDrop";
import Pagination from "../../components/Pagination";
import { useState } from "react";
import { HiFilter } from "react-icons/hi";
import { GrClose } from "react-icons/gr";

const listFilter = [
  {
    nameFil: "Dòng sản phẩm",
    choiceFil: ["Điện Thoại", "Ipad", "Đồng Hồ", "Máy Tính", "Máy Tính Bảng"],
  },
  {
    nameFil: "Khoảng Giá",
    choiceFil: [
      "Phổ thông (0₫ - 6.999.000 ₫)",
      "Trung cấp (7.000.000₫ - 14.999.000₫)",
      "Cao cấp (15.000.000₫ trở lên)",
    ],
  },
  {
    nameFil: "Bộ Nhớ",
    choiceFil: [
      "~4GB",
      " 8GB",
      "16GB",
      " 32GB",
      " 64GB",
      "128GB",
      "256GB",
      "512GB~",
    ],
  },
];

const sortOptions = [
  "Mới Nhất",
  "Phổ Biến Nhất",
  "Highest Rated",
  "Theo Khuyến Nghị",
  "Có Hàng Online",
  "Xem nhiều nhất",
];

const listProducts = [
  {
    name: "Galaxy Z Fold4",
    price: "20.000.000",
    image: "/images/devices/dt1.jpg",
  },
  {
    name: "Galaxy Z Flip4 Bespoke Edition",
    price: "20.000.000",
    image: "/images/devices/dt2.jpg",
  },
  {
    name: "Galaxy Z Flip4",
    price: "20.000.000",
    image: "/images/devices/dt3.jpg",
  },
  {
    name: "Galaxy Z Flip4 Bespoke Edition",
    price: "20.000.000",
    image: "/images/devices/dt4.jpg",
  },
  {
    name: "Galaxy Z Flip4 Bespoke Edition",
    price: "20.000.000",
    image: "/images/devices/dt5.jpg",
  },
  {
    name: "Galaxy Z Flip4 Bespoke Edition",
    price: "20.000.000",
    image: "/images/devices/dt6.jpg",
  },
];

const Product = () => {
  const [openModalMobile, setOpenModalMobile] = useState(false);

  return (
    <>
      <CustomHeader>
        <title>Tất cả các sản phẩm</title>
      </CustomHeader>
      <div className="mt-5">
        <div className="border-b mb-5 flex flex-col lg:flex-row items-start lg:items-center justify-between ">
          <h1 className="text-2xl lg:text-3xl font-bold mb-5">
            Tất cả sản phẩm
          </h1>
          <div className="flex items-center lg:w-fit w-full justify-between mb-2 lg:mb-0">
            <MenuDrop
              className="md:right-7 left-0 top-6 w-36"
              title="Theo Khuyến Nghị"
              options={sortOptions}
            />
            <HiFilter
              className="lg:hidden"
              onClick={() => setOpenModalMobile(true)}
            />
          </div>
        </div>
        <div className=" grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 md:gap-4 lg:gap-10">
          <Transition.Root show={openModalMobile} as={Fragment}>
            <Dialog
              as="div"
              className="relative z-40 lg:hidden"
              onClose={() => setOpenModalMobile(false)}
            >
              <Transition.Child
                as={Fragment}
                enter="transition-opacity ease-linear duration-300"
                enterFrom="opacity-0"
                enterTo="opacity-100"
                leave="transition-opacity ease-linear duration-300"
                leaveFrom="opacity-100"
                leaveTo="opacity-0"
              >
                <div className="fixed inset-0 bg-black bg-opacity-25" />
              </Transition.Child>

              <div className="fixed inset-0 z-40 flex">
                <Transition.Child
                  as={Fragment}
                  enter="transition ease-in-out duration-300 transform"
                  enterFrom="translate-x-full"
                  enterTo="translate-x-0"
                  leave="transition ease-in-out duration-300 transform"
                  leaveFrom="translate-x-0"
                  leaveTo="translate-x-full"
                >
                  <Dialog.Panel className="lg:hidden relative px-4 ml-auto flex h-full w-full max-w-xs flex-col overflow-y-auto bg-white py-4 pb-12 shadow-xl">
                    <div className="flex items-center justify-between border-b border-b-black mb-3">
                      <h2 className="text-lg font-medium text-gray-900">
                        Filters
                      </h2>
                      <button
                        type="button"
                        className="-mr-2 flex h-10 w-10 items-center justify-center rounded-md bg-white p-2 text-gray-400"
                        onClick={() => setOpenModalMobile(false)}
                      >
                        <span className="sr-only">Close menu</span>
                        <GrClose className="h-4 w-4" aria-hidden="true" />
                      </button>
                    </div>

                    {/* Filters */}
                    <div className={`${openModalMobile ? "" : "hidden"}`}>
                      {listFilter.map((filter, index) => (
                        <Disclosure key={index}>
                          {({ open }) => (
                            <div>
                              <Disclosure.Button className="flex items-cente w-full py-2 justify-between text-lg font-semibold">
                                <p>{filter.nameFil}</p>
                                <div className="mt-1">
                                  {open ? <IoIosArrowUp /> : <IoIosArrowDown />}
                                </div>
                              </Disclosure.Button>
                              {filter.choiceFil.map((choice, index) => (
                                <Disclosure.Panel
                                  key={index}
                                  className="hover:bg-gray-100 py-1"
                                >
                                  <input
                                    id={`filterProducts-${choice}-${index}`}
                                    type="checkbox"
                                    className="mr-3 cursor-pointer"
                                  />
                                  <label
                                    className="opacity-90 peer-hover:text-blue-500 cursor-pointer"
                                    htmlFor={`filter-devies-${choice}-${index}`}
                                  >
                                    {choice}
                                  </label>
                                </Disclosure.Panel>
                              ))}
                            </div>
                          )}
                        </Disclosure>
                      ))}
                    </div>
                  </Dialog.Panel>
                </Transition.Child>
              </div>
            </Dialog>
          </Transition.Root>
          <div className="col-span-1 hidden lg:block">
            {listFilter.map((filter, index) => (
              <Disclosure key={index}>
                {({ open }) => (
                  <div>
                    <Disclosure.Button className="flex items-cente w-full py-2 justify-between text-lg font-semibold">
                      <p>{filter.nameFil}</p>
                      <div className="mt-1">
                        {open ? <IoIosArrowUp /> : <IoIosArrowDown />}
                      </div>
                    </Disclosure.Button>
                    {filter.choiceFil.map((choice, index) => (
                      <Disclosure.Panel
                        key={index}
                        className="hover:bg-gray-100 py-1"
                      >
                        <input
                          id={`filter-devies-${choice}-${index}`}
                          type="checkbox"
                          className="mr-3 cursor-pointer"
                        />
                        <label
                          className="opacity-90 peer-hover:text-blue-500 cursor-pointer"
                          htmlFor={`filter-devies-${choice}-${index}`}
                        >
                          {choice}
                        </label>
                      </Disclosure.Panel>
                    ))}
                  </div>
                )}
              </Disclosure>
            ))}
          </div>
          <div className="col-span-1 lg:col-span-3 md:col-span-2 md:grid-cols-2 md:gap-4 lg:grid-cols-3 md:grid lg:gap-10">
            {listProducts.map((product, indexProduct) => (
              <div
                key={indexProduct}
                className="mt-5 col-span-1 bg-gray-100 rounded-xl"
              >
                <div className=" flex flex-col p-5 cursor-pointer">
                  <img
                    src={product.image}
                    className="w-[90%] mx-auto my-5"
                    alt=""
                  />
                  <div className="flex flex-col space-y-2">
                    <h2 className="font-bold text-xl mt-4 mb-2">
                      {product.name}
                    </h2>
                    <p className="font-bold">{product.price} đ</p>
                  </div>
                </div>
              </div>
            ))}
            <Pagination
              className="lg:col-span-3 md:col-span-2"
              current={0}
              pageSize={0}
              total={0}
              onChange={function (page: number): void {
                throw new Error("Function not implemented.");
              }}
            />
          </div>
        </div>
      </div>
    </>
  );
};

Product.getLayout = function getLayout(page: ReactElement) {
  return <Mainlayout>{page}</Mainlayout>;
};

export default Product;
