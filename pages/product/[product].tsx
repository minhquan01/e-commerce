import { RadioGroup, Tab } from "@headlessui/react";
import React from "react";
import { ReactElement } from "react";
import { CustomHeader } from "../../components/Header/CustomHeader";
import Mainlayout from "../../components/Layout/MainLayout";
import { useState } from "react";
import { BsCartPlus } from "react-icons/bs";
import { Swiper, SwiperSlide } from "swiper/react";
import { Pagination } from "swiper";

const product = {
  name: "Samsung Galaxy S22 Ultra",
  price: "23.000.000",
  memorys: ["128", "256"],
  colors: ["black", "green", "yellow", "purple", "white"],
  details: {
    id: "31232",
    display: "6.7",
    ram: "8GB",
    camera: "64/12/12",
  },
  images: [
    "/images/product-detail/img1.jpg",
    "/images/product-detail/img2.jpg",
    "/images/product-detail/img3.jpg",
    "/images/product-detail/img4.jpg",
  ],
  reviews: [
    {
      author: "Nguyễn Văn A",
      date: "04/11/2022",
      decs: "Sản phẩm tốt",
    },
    {
      author: "Nguyễn Văn A",
      date: "04/11/2022",
      decs: "Sản phẩm tốt",
    },
    {
      author: "Nguyễn Văn A",
      date: "04/11/2022",
      decs: "Sản phẩm tốt",
    },
    {
      author: "Nguyễn Văn A",
      date: "04/11/2022",
      decs: "Sản phẩm tốt",
    },
  ],
};

const listProducts = [
  {
    name: "Galaxy Z Fold4",
    price: "20.000.000",
    image: "/images/devices/dt1.jpg",
  },
  {
    name: "Galaxy Z Flip4 Bespoke Edition",
    price: "20.000.000",
    image: "/images/devices/dt2.jpg",
  },
  {
    name: "Galaxy Z Flip4",
    price: "20.000.000",
    image: "/images/devices/dt3.jpg",
  },
  {
    name: "Galaxy Z Flip4 Bespoke Edition",
    price: "20.000.000",
    image: "/images/devices/dt4.jpg",
  },
];

const Product = () => {
  const [selectedColor, setSelectedColor] = useState<string>(product.colors[0]);
  const [selectedMemory, setSelectedMemory] = useState<string>(
    product.memorys[0]
  );

  return (
    <div className="mt-10">
      {/* Preview */}
      <div className="lg:grid lg:grid-cols-8 lg:items-start gap-x-20">
        {/* Mobile */}
        <Swiper pagination={true} modules={[Pagination]} className="md:hidden">
          {product.images.map((image, index) => (
            <SwiperSlide key={index}>
              <img
                src={image}
                className="w-10/12 mx-auto mb-10 rounded-md"
                alt=""
              />
            </SwiperSlide>
          ))}
        </Swiper>
        {/* Image gallery */}
        <Tab.Group
          as="div"
          className="hidden col-span-4 md:flex flex-col-reverse"
        >
          {/* Image selector */}
          <div className="mx-auto mt-6 hidden w-full sm:block lg:max-w-none">
            <Tab.List className="grid grid-cols-4 grid-rows-1 gap-6">
              {product.images.map((image) => (
                <Tab
                  key={image}
                  className="row-span-1 relative flex h-24 cursor-pointer items-center justify-center rounded-md bg-white text-sm font-medium uppercase text-gray-900 hover:bg-gray-50 focus:outline-none focus:ring focus:ring-opacity-50 focus:ring-offset-4"
                >
                  {({ selected }) => (
                    <>
                      <span className="absolute inset-0 overflow-hidden border border-black border-opacity-20 p-2 rounded-md">
                        <img
                          src={image}
                          alt=""
                          className="h-full w-4/6 mx-auto  object-cover object-center"
                        />
                      </span>
                      <span
                        className={`${
                          selected ? "ring-indigo-300" : "ring-transparent"
                        }
                          pointer-events-none absolute inset-0 rounded-md ring-2 ring-offset-2`}
                        aria-hidden="true"
                      />
                    </>
                  )}
                </Tab>
              ))}
            </Tab.List>
          </div>

          <Tab.Panels className="aspect-w-1 aspect-h-1 w-full">
            {product.images.map((image) => (
              <Tab.Panel key={image}>
                <div className="border-2 border-opacity-20 border-gray-500 border-dashed rounded-lg">
                  <img
                    src={image}
                    className="h-full w-4/6 mx-auto py-2 object-cover object-center sm:rounded-lg"
                  />
                </div>
              </Tab.Panel>
            ))}
          </Tab.Panels>
        </Tab.Group>

        {/* Product info */}
        <div className=" flex flex-col space-y-5 col-span-3">
          <CustomHeader>
            <title>{product.name}</title>
          </CustomHeader>
          <h1 className="text-2xl md:text-5xl font-semibold">{product.name}</h1>
          <p className="font-bold text-base md:text-lg">{product.price}đ</p>
          <div>
            <p className="font-medium">Bộ nhớ</p>
            <div className="flex space-x-4">
              <RadioGroup
                value={selectedMemory}
                onChange={setSelectedMemory}
                className="mt-2"
              >
                <span className="flex items-center space-x-3">
                  {product.memorys.map((memory) => (
                    <RadioGroup.Option
                      key={memory}
                      value={memory}
                      className={({ checked }) =>
                        `
                  ${checked ? `border border-black border-opacity-80` : ""}
                    relative flex cursor-pointer rounded-md p-[1px] `
                      }
                    >
                      <p className="flex font-semibold px-2" key={memory}>
                        {memory}GB
                      </p>
                    </RadioGroup.Option>
                  ))}
                </span>
              </RadioGroup>
            </div>
          </div>
          <div className="flex font-semibold space-x-1">
            <p>Color:</p>
            <p>
              {selectedColor
                .substring(0, 1)
                .toUpperCase()
                .concat(selectedColor.substring(1, selectedColor.length))}
            </p>
          </div>
          <RadioGroup
            value={selectedColor}
            onChange={setSelectedColor}
            className="mt-2"
          >
            <span className="flex items-center space-x-3">
              {product.colors.map((color) => (
                <RadioGroup.Option
                  key={color}
                  value={color}
                  className={({ checked }) =>
                    `
                  ${
                    checked
                      ? `ring ring-offset-1 ring-black ring-opacity-40`
                      : ""
                  }
                    relative flex cursor-pointer rounded-full p-[1px] `
                  }
                >
                  <span
                    aria-hidden="true"
                    className="md:h-8 md:w-8 h-6 w-6 rounded-full"
                    style={{ backgroundColor: color }}
                  />
                </RadioGroup.Option>
              ))}
            </span>
          </RadioGroup>
          <div className="space-y-5">
            <p className="font-semibold">Thông tin chi tiết</p>

            <div className="flex flex-col space-y-3">
              <div className="flex justify-between border-b">
                <span className="text-gray-600">ID</span>
                <p>{product.details.id}</p>
              </div>
              <div className="flex justify-between border-b">
                <span className="text-gray-600">Màn hình</span>
                <p>{product.details.display}</p>
              </div>
              <div className="flex justify-between border-b">
                <span className="text-gray-600">RAM</span>
                <p>{product.details.ram}</p>
              </div>
              <div className="flex justify-between border-b">
                <span className="text-gray-600">Camera</span>
                <p>{product.details.camera}</p>
              </div>
            </div>
          </div>
          <div className="flex items-center justify-between md:justify-start md:space-x-5 font-semibold">
            <button className="flex space-x-2 border-blue-500  text-blue-500 rounded-lg py-3 px-4 border items-center">
              <BsCartPlus />
              <p>Thêm vào giỏ hàng</p>
            </button>
            <button className="bg-blue-500 rounded-lg py-3 px-4 text-white">
              Mua ngay
            </button>
          </div>
        </div>
      </div>
      {/* More */}
      <div className="flex flex-col mt-14">
        <p className="text-2xl font-semibold">Các sản phẩm khác</p>
        <div className="grid grid-cols-1 md:grid-cols-2 md:gap-x-8 lg:grid-cols-4 lg:gap-x-10">
          {listProducts.map((product, indexProduct) => (
            <div
              key={indexProduct}
              className="mt-5 col-span-1 bg-gray-100 rounded-xl"
            >
              <div className=" flex flex-col p-5 cursor-pointer">
                <img
                  src={product.image}
                  className="w-4/5 mx-auto my-5"
                  alt=""
                />
                <div className="flex flex-col space-y-2">
                  <h2 className="font-semibold text-lg mt-4 mb-2">
                    {product.name}
                  </h2>
                  <p className="font-semibold text-sm">{product.price} đ</p>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

Product.getLayout = function getLayout(page: ReactElement) {
  return <Mainlayout>{page}</Mainlayout>;
};
export default Product;
