import React from "react";
import { ReactElement } from "react";
import { CustomHeader } from "../components/Header/CustomHeader";
import { AiOutlineMinus, AiOutlinePlus } from "react-icons/ai";
import { IoClose } from "react-icons/io5";
import LayoutNotFooter from "../components/Layout/LayoutNotFooter";
import Link from "next/link";

const productCarts = [
  {
    image: "/images/devices/dt1.jpg",
    name: "Galaxy Y",
    color: "Red",
    memory: "128",
    price: "15.000.000",
    amount: "1",
  },
  {
    image: "/images/devices/dt2.jpg",
    name: "Galaxy Y",
    color: "Red",
    memory: "128",
    price: "15.000.000",
    amount: "1",
  },
  {
    image: "/images/devices/dt3.jpg",
    name: "Galaxy Y",
    color: "Red",
    memory: "128",
    price: "15.000.000",
    amount: "1",
  },
  {
    image: "/images/devices/dt4.jpg",
    name: "Galaxy Y",
    color: "Red",
    memory: "128",
    price: "15.000.000",
    amount: "1",
  },
  {
    image: "/images/devices/dt5.jpg",
    name: "Galaxy Y",
    color: "Red",
    memory: "128",
    price: "15.000.000",
    amount: "1",
  },
];

const Cart = () => {
  return (
    <>
      <CustomHeader>
        <title>Giỏ Hàng</title>
      </CustomHeader>
      <div>
        <h1 className="text-2xl xl:text-3xl font-bold mb-5">Giỏ hàng</h1>
        <div className="md:grid md:grid-cols-12 xl:px-20 md:gap-x-10 xl:gap-x-20 mt-2 xl:mt-10">
          {/* Thông tin đơn hàng */}
          <div className="col-span-7 space-y-5 xl:mr-10">
            {productCarts.map((product, index) => (
              <div
                key={index}
                className="relative border rounded-xl shadow-md grid grid-cols-3 py-5 px-2 gap-x-8"
              >
                <div className="absolute top-3 right-3 text-xl p-1 hover:text-red-500 cursor-pointer">
                  <IoClose />
                </div>
                <img
                  className="col-span-1 mx-auto w-full h-full xl:w-3/4 object-cover rounded-lg"
                  src={product.image}
                  alt=""
                />
                <div className="col-span-2">
                  <div>
                    <h2 className="text-2xl font-semibold">{product.name}</h2>
                    <p className="opacity-75 text-sm">{`${product.color}-${product.memory}GB`}</p>
                  </div>
                  <p className="font-semibold">{product.price}đ</p>
                  <div className="bg-zinc-200 w-20 rounded-md px-2 py-1 mt-5">
                    <div className="flex justify-between items-center">
                      <AiOutlineMinus className="text-blue-600 cursor-pointer" />
                      <p>{product.amount}</p>
                      <AiOutlinePlus className="text-blue-600 cursor-pointer" />
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </div>
          {/* Checkout */}
          <div className="col-span-5 mt-6 md:mt-0 bg-gray-100 h-fit py-5 px-6 rounded-lg">
            <h3 className="font-medium text-lg">Tính tiền đơn hàng</h3>
            <div className="border-b flex justify-between items-center py-3">
              <span className="text-stone-700 ">Tổng đơn hàng</span>
              <p>50.000.000đ</p>
            </div>
            <div className="border-b flex justify-between items-center py-3">
              <span className="text-stone-700 ">Tiền ship</span>
              <p>200.000đ</p>
            </div>
            <div className="font-medium text-lg flex justify-between mt-2">
              <h2>Tổng</h2>
              <p>50.200.000đ</p>
            </div>
            <Link href="/checkout">
              <button className="flex justify-center items-center w-full bg-blue-500 py-3 rounded-lg text-white font-semibold text-lg mt-5">
                Thanh Toán
              </button>
            </Link>
          </div>
        </div>
      </div>
    </>
  );
};

Cart.getLayout = function getLayout(page: ReactElement) {
  return <LayoutNotFooter>{page}</LayoutNotFooter>;
};

export default Cart;
