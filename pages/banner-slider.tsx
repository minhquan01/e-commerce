import React, { useCallback } from "react";
import { ReactElement } from "react";
import ContentAdmin from "../components/Layout/ContentAdmin";
import MainAdmin from "../components/Layout/MainAdmin";
import UploadBannnerSlider from "../containers/Upload/UploadBannnerSlider";
import {
  getBannerSlider,
  publishBannerSlider,
  unPublishBannerSlider,
} from "../services/image";
import { useState, useEffect } from "react";
import Card from "../components/Card";
import Pagination from "../components/Pagination";
import { useRouter } from "next/router";
import { BannerSlider, GetBannerQuery } from "../interfaces/image";
import dateFormat from "dateformat";
import Modal from "../components/Modal";
import { AiOutlinePlus } from "react-icons/ai";
import { BsCheck2 } from "react-icons/bs";
import { IoMdClose } from "react-icons/io";
import { toast } from "react-toastify";
import LoadingPage from "../components/loading/LoadingPage";

const Banner = () => {
  const DEFAULT_BANNER_LIMIT = 6;
  const router = useRouter();
  const [dataImage, setDataImage] = useState([]);
  const [totalImage, setTotalImage] = useState(0);
  const [openModal, setOpenModal] = useState(false);
  const [selectedImage, setSelectedImage] = useState<any>([]);
  const [selectedId, setSelectedId] = useState<any>([]);
  const [loading, setLoading] = useState(true);

  const dataBannerSlider = async (query?: GetBannerQuery): Promise<void> => {
    try {
      const { data } = await getBannerSlider({
        ...query,
        limit: DEFAULT_BANNER_LIMIT,
      });
      setDataImage(data.result);
      setTotalImage(data.total);
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  };

  const onChangePage = (page: number) => {
    router.push({
      query: {
        page: page,
      },
    });
  };

  useEffect(() => {
    const query = handleQueryParams(router.query);
    dataBannerSlider(query);
  }, [router.query, openModal]);

  const handleQueryParams = (query: any) => {
    const newQuery = query;

    if (!newQuery.page) {
      query.page = "1";
      router.push(
        {
          query: {
            ...newQuery,
          },
        },
        undefined,
        { shallow: true }
      );
    }
    return newQuery;
  };

  const handlePublishBanner = async () => {
    const res = await publishBannerSlider(selectedId);
    if (res.status === 200) {
      toast.success("Đã Publish");
    }
    dataBannerSlider();
    setSelectedId([]);
    setSelectedImage([]);
  };

  const handleUnPublishBanner = async () => {
    const res = await unPublishBannerSlider(selectedId);
    if (res.status === 200) {
      toast.success("Đã UnPublish");
    }
    dataBannerSlider();
    setSelectedId([]);
    setSelectedImage([]);
  };

  const handleSelectBanner = useCallback(
    (image: BannerSlider) => {
      if (selectedId.length !== 0 && selectedImage.includes(image)) {
        const res = selectedImage.filter(
          (item: BannerSlider) => item !== image
        );
        setSelectedImage(res);
        const resId = selectedId.filter((item: string) => item !== image.id);
        setSelectedId(resId);
      } else {
        setSelectedImage([...selectedImage, image]);
        setSelectedId([...selectedId, image.id]);
      }
    },
    [selectedImage, selectedId]
  );

  const checkImageSelected = useCallback(
    (image: BannerSlider) => {
      return selectedImage.some((item: BannerSlider) => {
        return item.id === image.id;
      });
    },
    [selectedImage]
  );


  return (
    <>
      {loading && <LoadingPage />}
      <ContentAdmin title="Banner Slider">
        <div className="flex justify-between items-center mb-5">
          <div className="flex space-x-1 items-center">
            <button
              onClick={handleUnPublishBanner}
              className="flex items-center space-x-1 px-3  py-1 rounded-lg  font-semibold bg-blue-500 hover:bg-blue-600 text-white"
            >
              <IoMdClose />
              <p>UnPublish</p>
            </button>
            <button
              onClick={handlePublishBanner}
              className="flex items-center space-x-1 px-3  py-1 rounded-lg  font-semibold bg-blue-500 hover:bg-blue-600 text-white"
            >
              <BsCheck2 />
              <p>Publish</p>
            </button>
          </div>
          <button
            onClick={() => setOpenModal(true)}
            className="flex items-center space-x-1 px-3  py-1 rounded-lg  font-semibold bg-blue-500 hover:bg-blue-600 text-white"
          >
            <AiOutlinePlus />
            <p>Thêm ảnh</p>
          </button>
        </div>
        <Card>
          <Card.Content>
            <div className="grid grid-cols-3 gap-4">
              {dataImage.map((item: BannerSlider, idx) => {
                return (
                  <div
                    onClick={() => handleSelectBanner(item)}
                    className={`relative flex rounded-md cursor-pointer p-1 flex-col items-center space-y-1 ${
                      checkImageSelected(item) ? "border-4 border-blue-500" : ""
                    }`}
                    key={idx}
                  >
                    {item.publish && (
                      <p className="bg-blue-400 text-white rounded-md px-1 absolute top-1 right-2">
                        Publish
                      </p>
                    )}
                    <img
                      src={item.url}
                      className="rounded-md w-full h-[124px] object-cover"
                      alt=""
                    />
                    <p>{dateFormat(item.createdAt, "dd/mm/yyyy")}</p>
                  </div>
                );
              })}
            </div>
          </Card.Content>
          <Pagination
            current={Number(router.query.page || 1)}
            pageSize={DEFAULT_BANNER_LIMIT}
            total={totalImage}
            onChange={onChangePage}
          />
        </Card>
      </ContentAdmin>
      <Modal title="Thêm hình ảnh" open={openModal} setOpen={setOpenModal}>
        <UploadBannnerSlider open={openModal} setOpen={setOpenModal} />
      </Modal>
    </>
  );
};

Banner.getLayout = function getLayout(page: ReactElement) {
  return <MainAdmin>{page}</MainAdmin>;
};

export default Banner;
