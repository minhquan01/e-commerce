import React from "react";
import { useState, useEffect, useMemo } from "react";
import ContentAdmin from "../components/Layout/ContentAdmin";
import { GetUsersQuery, ListUser } from "../interfaces/user";
import { getAllUser } from "../services/use";
import { useRouter } from "next/router";
import Role from "../containers/Role";
import Table from "../components/Table";
import Pagination from "../components/Pagination";
import Card from "../components/Card";
import dateFormat from "dateformat";

const DEFAULT_USERS_LIMIT = 6;

const User = () => {
  const column = ["Số thứ tự", "Tên", "Email", "Ngày tạo", "Admin"];
  const [loading, setLoading] = useState(true);
  const [users, setUsers] = useState([]);
  const [totalUsers, setTotalUsers] = useState(0);
  const router = useRouter();

  let count = DEFAULT_USERS_LIMIT * (Number(router.query.page) - 1) + 1;

  const fetchUsers = async (query?: GetUsersQuery): Promise<void> => {
    try {
      const { data } = await getAllUser({
        ...query,
        limit: DEFAULT_USERS_LIMIT,
      });

      setUsers(data.users);
      setTotalUsers(data.total);
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  };

  const onChangePage = (page: number) => {
    router.push({
      query: {
        page: page,
      },
    });
  };

  useEffect(() => {
    const query = handleQueryParams(router.query);
    fetchUsers(query);
  }, [router.query]);

  const handleQueryParams = (query: any) => {
    const newQuery = query;

    if (!newQuery.page) {
      query.page = "1";
      router.push(
        {
          query: {
            ...newQuery,
          },
        },
        undefined,
        { shallow: true }
      );
    }
    return newQuery;
  };

  const dataSource = useMemo(() => {
    return users.map((item: ListUser, index: number) => {
      return [
        <> {count === 0 ? index + 1 : count++}</>,
        item.firstName + " " + item.lastName,
        item.email,
        <>{dateFormat(item.createdAt, "HH:MM dd/mm/yyyy")}</>,
        <>
          <Role role={item.admin} id={item.id} />
        </>,
      ];
    });
  }, [users]);

  return (
    <>
      <ContentAdmin title="Quản lý tài khoản">
        <title>Quản lý tài khoản</title>
      </ContentAdmin>
      <Card>
        <Card.Content>
          <Table columns={column} dataSource={dataSource} loading={loading} />
        </Card.Content>
        <Pagination
          current={Number(router.query.page || 1)}
          pageSize={DEFAULT_USERS_LIMIT}
          total={totalUsers}
          onChange={onChangePage}
        />
      </Card>
    </>
  );
};

export default User;
