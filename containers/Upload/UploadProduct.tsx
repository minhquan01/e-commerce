import React, { useRef } from "react";
import { toast } from "react-toastify";
import { uploadBannerSlider } from "../../services/image";
import { useState, useEffect } from "react";
import { AiOutlineCloudUpload } from "react-icons/ai";
import Button from "../../components/Button";
import { IoIosClose } from "react-icons/io";
import { addProducts } from "../../services/product";
import { ListProduct } from "../../interfaces/product";
import { editProducts } from "./../../services/product";

interface UploadProductProps {
  open: boolean;
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
  setAdd?: React.Dispatch<React.SetStateAction<boolean>>;
  dataEdit?: ListProduct;
  openAdd?: boolean;
}

const UploadProduct = ({
  open,
  setOpen,
  dataEdit,
  openAdd,
  setAdd,
}: UploadProductProps) => {
  const [imageSrc, setImageSrc] = useState("");
  const [uploadData, setUploadData] = useState();
  const [loadingBtn, setLoadingBtn] = useState(false);
  const [loadingMultiple, setLoadingMultiple] = useState(false);
  const [color, setColor] = useState("");
  const [memory, setMemory] = useState("");
  const [listColor, setListColor] = useState<string[]>([]);
  const [nameProd, setNameProd] = useState("");
  const [priceProd, setPriceProd] = useState("");
  const [memoryProd, setMemoryProd] = useState<string[]>([]);
  const [screenProd, setScreenProd] = useState("");
  const [cameraProd, setCameraProd] = useState("");
  const [idProdEdit, setIdProdEdit] = useState("");
  const [urlProd, setUrlProd] = useState("");
  const [preview, setPreview] = useState<string[]>([]);
  const [listImage, setListImage] = useState<string[]>([]);

  const memoryRef = useRef<any>();

  const handleOnChange = (changeEvent: any) => {
    const reader = new FileReader();

    reader.onload = function (onLoadEvent: any) {
      setImageSrc(onLoadEvent.target.result);
      setUploadData(undefined);
    };

    reader.readAsDataURL(changeEvent.target.files[0]);
  };

  const handleOnSubmit = async (event: any) => {
    setLoadingBtn(true);
    event.preventDefault();
    const form = event.currentTarget;
    const fileInput: any = Array.from(form.elements).find(
      ({ name }: any) => name === "file"
    );
    const formData = new FormData();

    for (const file of fileInput.files) {
      formData.append("file", file);
    }
    formData.append("upload_preset", "e-commerce");

    const data = await fetch(
      "https://api.cloudinary.com/v1_1/dd4way43x/image/upload",
      {
        method: "POST",
        body: formData,
      }
    ).then((r) => r.json());

    setUrlProd(data.url);
    setLoadingBtn(false);
  };

  const handleAddColor = () => {
    if (!listColor.includes(color)) {
      setListColor([...listColor, color]);
    } else {
      return;
    }
  };

  const handleAddMemory = () => {
    if (!memoryProd.includes(memory)) {
      setMemoryProd([...memoryProd, memory]);
    } else {
      return;
    }
    memoryRef.current.value = "";
    memoryRef.current.focus();
  };

  const handleDeteleColor = (item: string) => {
    setListColor(listColor.filter((items) => items !== item));
  };

  const handleDeteleMemory = (item: string) => {
    setMemoryProd(memoryProd.filter((items) => items !== item));
  };

  const handleMultipleImage = (e: any) => {
    const files = e.target.files;
    let previewArr: string[] = [];
    for (var i = 0; i < files.length; i++) {
      const reader = new FileReader();
      reader.readAsDataURL(files[i]);
      reader.onload = function (onLoadEvent: any) {
        previewArr.push(onLoadEvent.target.result);
        setPreview(preview.concat(previewArr));
      };
    }
  };

  const handleCancel = () => {
    setOpen(false);
    if (setAdd) {
      setAdd(false);
    }
  };

  const handleOnSubmitMultiple = async (event: any) => {
    setLoadingMultiple(true);
    event.preventDefault();
    const form = event.currentTarget;
    const fileInput: any = Array.from(form.elements).find(
      ({ name }: any) => name === "multiple-file"
    );
    const formData = new FormData();
    let dataUrl: string[] = [];

    for (const file of fileInput.files) {
      formData.append("file", file);
      formData.append("upload_preset", "e-commerce");
      const data = await fetch(
        "https://api.cloudinary.com/v1_1/dd4way43x/image/upload",
        {
          method: "POST",
          body: formData,
        }
      ).then((r) => r.json());
      dataUrl.push(data.url);
    }
    setListImage(dataUrl);
    setLoadingMultiple(false);
  };

  const handleAddProduct = async () => {
    const product = {
      idProdEdit,
      urlProd,
      nameProd,
      priceProd,
      memoryProd,
      screenProd,
      cameraProd,
      listColor,
      listImage,
    };
    if (idProdEdit) {
      const res = await editProducts(product);
      if (res.status === 200) {
        toast.success("Sửa phẩm thành công!");
      }
    } else {
      const res = await addProducts(product);
      if (res.status === 200) {
        toast.success("Thêm sản phẩm thành công!");
      }
    }
    setLoadingBtn(false);
    if (setAdd) {
      setAdd(false);
    }
    setOpen(false);
  };
  useEffect(() => {
    if (dataEdit) {
      setUrlProd(dataEdit.url);
      setNameProd(dataEdit.productDetail.name);
      setPriceProd(dataEdit.productDetail.price);
      setMemoryProd(dataEdit.productDetail.memory);
      setScreenProd(dataEdit.productDetail.screen);
      setCameraProd(dataEdit.productDetail.camera);
      setListColor(dataEdit.productDetail.color);
      setListImage(dataEdit.productDetail.imageDetail);
      setImageSrc(dataEdit.url);
      setPreview(dataEdit.productDetail.imageDetail);
      setIdProdEdit(dataEdit.id);
    }
  }, [dataEdit]);

  useEffect(() => {
    if (open === false || openAdd) {
      setImageSrc("");
      setPreview([]);
      setUrlProd("");
      setNameProd("");
      setPriceProd("");
      setMemoryProd([]);
      setScreenProd("");
      setCameraProd("");
      setListColor([]);
      setListImage([]);
      setImageSrc("");
      setIdProdEdit("");
    }
  }, [open, openAdd]);

  return (
    <div className="px-6 h-fit max-h-[500px] overflow-y-auto w-[800px]">
      <form
        method="post"
        onChange={(e) => handleOnChange(e)}
        onSubmit={(e) => handleOnSubmit(e)}
      >
        <div>
          <h3 className="text-base font-medium leading-6 text-gray-900">Ảnh</h3>
          <p className="mt-1 text-sm text-gray-500">Ảnh nổi bật của sản phẩm</p>
          <div className="mt-2 flex justify-center items-center w-full">
            <label
              htmlFor="dropzone-file"
              className="flex flex-col justify-center items-center w-3/4 h-32 rounded-lg border-2 border-blue-300 border-dashed cursor-pointer"
            >
              <div className="flex flex-col justify-center items-center pt-5 pb-6">
                <AiOutlineCloudUpload className="text-4xl text-blue-400" />
                <p className="mb-2 text-sm">
                  <span className="font-semibold">Click to upload</span> or drag
                  and drop
                </p>
                <p className="text-xs text-gray-500 dark:text-gray-400">
                  SVG, PNG, JPG or GIF (Max 15MB)
                </p>
              </div>
              <input
                name="file"
                id="dropzone-file"
                type="file"
                className="hidden"
                accept="image/*"
              />
            </label>
          </div>
        </div>

        {imageSrc && (
          <>
            <p>Preview</p>
            <img
              className="mx-auto rounded-lg h-[250px] w-[600px] object-cover"
              src={imageSrc}
            />
          </>
        )}
        {imageSrc && !uploadData && (
          <div className="flex flex-col w-full justify-center items-center mt-5">
            <i className="text-red-400 text-sm opacity-70 mb-1">
              *Vui lòng upload sau khi xem preview
            </i>
            <Button
              submit
              loading={loadingBtn}
              label="Upload"
              className=" px-2 py-1 bg-blue-500 text-white rounded-lg hover:bg-blue-600"
            ></Button>
          </div>
        )}
        {uploadData && (
          <code>
            <pre>{JSON.stringify(uploadData, null, 2)}</pre>
          </code>
        )}
      </form>

      <div className="mt-5 mb-4">
        <div>
          <h3 className="text-base font-medium leading-6 text-gray-900">
            Chi tiết
          </h3>
          <p className="mt-1 text-sm text-gray-500">
            Điền các thông tin chi tiết của sản phẩm
          </p>
        </div>
        <div className="mt-6 grid grid-cols-1 gap-y-6 gap-x-4 sm:grid-cols-6">
          <div className="sm:col-span-3">
            <label
              htmlFor="name"
              className="block text-sm font-medium text-gray-700"
            >
              Tên sản phẩm
            </label>
            <div className="mt-1">
              <input
                onChange={(e) => setNameProd(e.target.value)}
                value={nameProd}
                type="text"
                name="name"
                id="name"
                className="block w-full rounded-md border-gray-400 shadow-sm border outline-blue-400 px-1 py-1 sm:text-sm"
              />
            </div>
          </div>

          <div className="sm:col-span-3">
            <label
              htmlFor="price"
              className="block text-sm font-medium text-gray-700"
            >
              Giá sản phẩm
            </label>
            <div className="mt-1">
              <div className=" mt-1 flex rounded-md shadow-sm border border-gray-400 ">
                <input
                  onChange={(e) => setPriceProd(e.target.value)}
                  value={priceProd}
                  type="text"
                  name="price"
                  id="price"
                  className="block w-full rounded-md outline-none shadow-sm px-1 py-1 sm:text-sm"
                />
                <span className="inline-flex items-center rounded-r-md bg-gray-50 px-3 text-gray-500 sm:text-sm">
                  đồng
                </span>
              </div>
            </div>
          </div>

          <div className="sm:col-span-3">
            <label
              htmlFor="memory"
              className="block text-sm font-medium text-gray-700"
            >
              Bộ nhớ
            </label>
            <div className="mt-1 flex space-x-2">
              <div className=" flex-1 flex rounded-md shadow-sm border border-gray-400 ">
                <input
                  onChange={(e) => setMemory(e.target.value)}
                  ref={memoryRef}
                  type="text"
                  name="memory"
                  id="memory"
                  className="block w-full rounded-md outline-none shadow-sm px-1 py-1 sm:text-sm"
                />
                <span className="inline-flex items-center rounded-r-md bg-gray-50 px-3 text-gray-500 sm:text-sm">
                  GB
                </span>
              </div>
              <button
                onClick={handleAddMemory}
                className="rounded-md px-2 text-white font-semibold bg-blue-400 hover:bg-blue-500"
              >
                Thêm
              </button>
            </div>
            <div className="flex space-x-1 mt-2">
              {memoryProd.map((item, index) => (
                <p
                  className="relative bg-gray-300 w-fit px-2 rounded-xl group"
                  key={index}
                >
                  {item}
                  <IoIosClose
                    className="absolute -top-1 -right-1 hidden group-hover:block hover:text-red-500 cursor-pointer"
                    onClick={() => handleDeteleMemory(item)}
                  />
                </p>
              ))}
            </div>
          </div>

          <div className="sm:col-span-3">
            <label
              htmlFor="screen"
              className="block text-sm font-medium text-gray-700"
            >
              Màn hình
            </label>
            <div className="mt-1">
              <div className=" mt-1 flex rounded-md shadow-sm border border-gray-400 ">
                <input
                  onChange={(e) => setScreenProd(e.target.value)}
                  type="text"
                  value={screenProd}
                  name="screen"
                  id="screen"
                  className="block w-full rounded-md outline-none shadow-sm px-1 py-1 sm:text-sm"
                />
                <span className="inline-flex items-center rounded-r-md bg-gray-50 px-3 text-gray-500 sm:text-sm">
                  inch
                </span>
              </div>
            </div>
          </div>

          <div className="sm:col-span-3">
            <label
              htmlFor="camera"
              className="block text-sm font-medium text-gray-700"
            >
              Camera
            </label>
            <div className="mt-1">
              <input
                onChange={(e) => setCameraProd(e.target.value)}
                type="text"
                name="camera"
                value={cameraProd}
                id="camera"
                className="block w-full rounded-md border-gray-400 shadow-sm border outline-blue-400 px-1 py-1 sm:text-sm"
              />
            </div>
          </div>

          <div className="sm:col-span-3 ">
            <div>
              <label
                htmlFor="color"
                className="block text-sm font-medium text-gray-700"
              >
                Màu sắc
              </label>
              <div className="mt-1 flex items-center space-x-2">
                <select
                  onChange={(e) => setColor(e.target.value)}
                  id="color"
                  name="color"
                  className=" block w-full rounded-md  border-gray-400 shadow-sm border focus:border-blue-400 px-1 py-1 sm:text-sm"
                >
                  <option>Chọn màu</option>
                  <option>Red</option>
                  <option>Blue</option>
                  <option>Green</option>
                  <option>Yellow</option>
                  <option>Orange</option>
                  <option>Black</option>
                  <option>White</option>
                </select>
                <button
                  onClick={handleAddColor}
                  className="rounded-md py-[3px] px-2 text-white font-semibold bg-blue-400 hover:bg-blue-500"
                >
                  Thêm
                </button>
              </div>
              <div className="flex space-x-1 mt-2">
                {listColor.map((item, index) => (
                  <p
                    className="relative bg-gray-300 w-fit px-2 rounded-xl group"
                    key={index}
                  >
                    {item}
                    <IoIosClose
                      className="absolute -top-1 -right-1 hidden group-hover:block hover:text-red-500 cursor-pointer"
                      onClick={() => handleDeteleColor(item)}
                    />
                  </p>
                ))}
              </div>
            </div>
          </div>
          <form
            method="POST"
            className="col-span-6"
            onChange={(e) => handleMultipleImage(e)}
            onSubmit={(e) => handleOnSubmitMultiple(e)}
          >
            <h1 className="text-sm font-medium text-gray-700">Ảnh chi tiết</h1>
            <div className="mt-2 flex justify-center items-center w-full ">
              <label
                htmlFor="multiple-file"
                className="flex flex-col justify-center items-center w-3/4 h-32 rounded-lg border-2 border-blue-300 border-dashed cursor-pointer"
              >
                <div className="flex flex-col justify-center items-center pt-5 pb-6">
                  <AiOutlineCloudUpload className="text-4xl text-blue-400" />
                  <p className="mb-2 text-sm">
                    <span className="font-semibold">Click to upload</span> or
                    drag and drop
                  </p>
                  <p className="text-xs text-gray-500 dark:text-gray-400">
                    SVG, PNG, JPG or GIF (Max 15MB)
                  </p>
                </div>
                <input
                  name="multiple-file"
                  id="multiple-file"
                  type="file"
                  className="hidden"
                  multiple
                  accept="image/*"
                />
              </label>
            </div>
            <div>
              {preview.length > 0 && (
                <h1 className="mt-2 mb-1 text-sm font-medium text-gray-700">
                  Preview
                </h1>
              )}
              <div className="flex items-center justify-center space-x-2">
                {preview.length > 0 &&
                  preview.map((item, index) => (
                    <div key={index}>
                      <img
                        className="w-[300px] h-[307px] rounded-md object-cover"
                        src={item}
                        alt=""
                      />
                    </div>
                  ))}
              </div>
            </div>
            {preview.length > 0 && (
              <div className="flex flex-col w-full justify-center items-center mt-5">
                <i className="text-red-400 text-sm opacity-70 mb-1">
                  *Vui lòng upload sau khi xem preview
                </i>
                <Button
                  submit
                  loading={loadingMultiple}
                  label="Upload"
                  className=" px-2 py-1 bg-blue-500 text-white rounded-lg hover:bg-blue-600"
                ></Button>
              </div>
            )}
          </form>
        </div>
      </div>

      <div className="w-full space-x-2 items-center text-center">
        <button
          onClick={handleCancel}
          className="bg-gray-300 hover:bg-gray-400 rounded-md px-2 py-1 font-semibold text-gray-900"
        >
          Cancel
        </button>
        <button
          onClick={handleAddProduct}
          className="bg-blue-500 hover:bg-blue-600 rounded-md px-2 py-1 font-semibold text-white"
        >
          Save
        </button>
      </div>
    </div>
  );
};

export default UploadProduct;
