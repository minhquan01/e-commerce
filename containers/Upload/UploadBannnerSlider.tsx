import React from "react";
import { toast } from "react-toastify";
import { uploadBannerSlider } from "../../services/image";
import { useState, useEffect } from "react";
import { AiOutlineCloudUpload } from "react-icons/ai";
import Button from "../../components/Button";

interface UploadBannnerSliderProps {
  open: boolean;
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
}

const UploadBannnerSlider = ({ open, setOpen }: UploadBannnerSliderProps) => {
  const [imageSrc, setImageSrc] = useState();
  const [uploadData, setUploadData] = useState();
  const [loadingBtn, setLoadingBtn] = useState(false);

  const handleOnChange = (changeEvent: any) => {
    const reader = new FileReader();

    reader.onload = function (onLoadEvent: any) {
      setImageSrc(onLoadEvent.target.result);
      setUploadData(undefined);
    };

    reader.readAsDataURL(changeEvent.target.files[0]);
  };

  const handleOnSubmit = async (event: any) => {
    setLoadingBtn(true);
    event.preventDefault();
    const form = event.currentTarget;
    const fileInput: any = Array.from(form.elements).find(
      ({ name }: any) => name === "file"
    );
    const formData = new FormData();

    for (const file of fileInput.files) {
      formData.append("file", file);
    }
    formData.append("upload_preset", "e-commerce");

    const data = await fetch(
      "https://api.cloudinary.com/v1_1/dd4way43x/image/upload",
      {
        method: "POST",
        body: formData,
      }
    ).then((r) => r.json());
    const res = await uploadBannerSlider(data.url);
    if (res.status === 200) {
      toast.success("Tải lên ảnh thành cônng!");
    }
    setLoadingBtn(false);
    setOpen(false);
  };

  useEffect(() => {
    setImageSrc(undefined);
  }, [open]);

  return (
    <div className="h-fit max-h-[500px] w-[800px]">
      <form
        method="post"
        onChange={(e) => handleOnChange(e)}
        onSubmit={(e) => handleOnSubmit(e)}
      >
        <div className="flex justify-center items-center w-full">
          <label
            htmlFor="dropzone-file"
            className="flex flex-col justify-center items-center w-3/4 h-32 rounded-lg border-2 border-blue-300 border-dashed cursor-pointer"
          >
            <div className="flex flex-col justify-center items-center pt-5 pb-6">
              <AiOutlineCloudUpload className="text-4xl text-blue-400" />
              <p className="mb-2 text-sm">
                <span className="font-semibold">Click to upload</span> or drag
                and drop
              </p>
              <p className="text-xs text-gray-500 dark:text-gray-400">
                SVG, PNG, JPG or GIF (Max 15MB)
              </p>
            </div>
            <input
              name="file"
              id="dropzone-file"
              type="file"
              accept="image/png, image/gif, image/jpeg"
              className="hidden"
            />
          </label>
        </div>
        {imageSrc && (
          <>
            <p>Preview</p>
            <img
              className="mx-auto rounded-lg h-[250px] w-[600px] object-cover"
              src={imageSrc}
            />
          </>
        )}
        {imageSrc && !uploadData && (
          <div className="flex w-full justify-center items-center mt-5">
            <Button
              submit
              loading={loadingBtn}
              label="Upload"
              className=" px-2 py-1 bg-blue-500 text-white rounded-lg hover:bg-blue-600"
            ></Button>
          </div>
        )}
        {uploadData && (
          <code>
            <pre>{JSON.stringify(uploadData, null, 2)}</pre>
          </code>
        )}
      </form>
    </div>
  );
};

export default UploadBannnerSlider;
