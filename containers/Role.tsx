import React, { useState } from "react";
import { updateAdmin } from "../services/use";
import { toast } from "react-toastify";
type SelectChangeEventHandler = React.ChangeEventHandler<HTMLSelectElement>;
interface IProps {
  role: boolean;
  id: string;
}
const Role: React.FC<IProps> = (props) => {
  const { role, id } = props;

  const [selected, setSelected] = useState<boolean>(role);

  const handleChange: SelectChangeEventHandler = async (event) => {
    if (event.target.value === "admin") {
      setSelected(true);
    }
    if (event.target.value === "user") {
      setSelected(false);
    }

    const user = {
      id: id,
      role: selected,
    };
    const res = await updateAdmin(user);
    if (res.status === 200) {
      toast.success("Cập nhật quyền thành công!");
    }
  };

  return (
    <div>
      <div className="relative">
        <select
          className="bg-gray-50 border border-gray-300  text-sm rounded-md focus:ring-blue-500 focus:border-blue-500 block py-1 px-1 "
          defaultValue={role ? "admin" : "user"}
          onChange={handleChange}
        >
          <option value="admin">Admin</option>
          <option value="user">Users</option>
        </select>
      </div>
    </div>
  );
};

export default Role;
