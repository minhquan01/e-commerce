import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import { Autoplay, EffectFade, Navigation, Pagination } from "swiper";
import Image from "next/image";
import { getBannerPublish } from "../services/image";
import { useState, useEffect } from "react";
import { BannerSlider } from "../interfaces/image";

const SliderHome = () => {
  const [dataImage, setDataImage] = useState([]);

  const dataBanner = async () => {
    try {
      const { data } = await getBannerPublish();
      setDataImage(data);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    dataBanner();
  }, []);

  return (
    <div className="h-screen object-cover -mb-20">
      <Swiper
        spaceBetween={30}
        effect={"fade"}
        navigation={true}
        pagination={{
          dynamicBullets: true,
        }}
        loop={true}
        autoplay={{
          delay: 2500,
          disableOnInteraction: false,
        }}
        modules={[Autoplay, EffectFade, Navigation, Pagination]}
      >
        {dataImage.map((item: BannerSlider, idx) => (
          <SwiperSlide key={idx}>
            <img src={item.url} className="w-full h-full object-cover" />
          </SwiperSlide>
        ))}
      </Swiper>
    </div>
  );
};

export default SliderHome;
