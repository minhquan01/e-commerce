/* eslint-disable react/no-unescaped-entities */
import Link from "next/link";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { register } from "../redux/apiReq";
import { useRouter } from "next/router";
import { CustomHeader } from "../components/Header/CustomHeader";
import { AiOutlineEyeInvisible, AiOutlineEye } from "react-icons/ai";
import validator from "validator";
import { MessValidatorRegis } from "../interfaces";

const SignUp = () => {
  const [firstName, setFirstName] = useState<string>("");
  const [lastName, setLastName] = useState<string>("");
  const [email, setEmail] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [password2, setPassword2] = useState<string>("");
  const [hiddenPassword, setHiddenPassword] = useState("password");
  const [validatorMess, setValidatorMess] = useState<MessValidatorRegis>();

  const dispatch = useDispatch();
  const router = useRouter();

  const errMail = useSelector(
    (state: any) => state.auth.register.error?.response.data
  );

  const validatorForm = () => {
    const mess: any = {};

    if (validator.isEmpty(email)) {
      mess.email = "Không được bỏ trống Email";
    }
    if (validator.isEmpty(firstName)) {
      mess.firstName = "Không được bỏ trống Họ";
    }
    if (validator.isEmpty(lastName)) {
      mess.lastName = "Không được bỏ trống Tên";
    }
    if (validator.isEmpty(password)) {
      mess.password = "Không được bỏ trống mật khẩu";
    }
    if (!validator.isEmail(email)) {
      mess.email = "Email không hợp lệ";
    }
    if (errMail) {
      mess.email = errMail;
    }
    if (!validator.isLength(password, { min: 6 })) {
      mess.password = "Mật khẩu cần tối thiểu 6 ký tự";
    }
    if (!validator.matches(password, password2)) {
      mess.password2 = "Mật khẩu không khớp";
    }

    setValidatorMess(mess);
    if (Object.keys(mess).length > 0) return true;
    return false;
  };

  const handleRegister = async () => {
    const valid = validatorForm();

    if (!valid) {
      try {
        const newAcc = {
          firstName: firstName,
          lastName: lastName,
          email: email,
          password: password,
        };
        await register(newAcc, dispatch, router);
      } catch (error) {
        console.log(error);
      }
    }
  };

  return (
    <>
      <CustomHeader>
        <title>Đăng ký </title>
      </CustomHeader>
      <div className="bg-[#171717] w-4/5 xl:w-2/5 shadow-2xl rounded-xl py-8 md:py-12 text-xl mt-14 px-5 md:px-12 flex flex-col justify-center z-50">
        <div className="space-y-2">
          <h1 className=" text-gray-200 text-2xl sm:font-medium sm:text-3xl text-center">
            Tạo tài khoản Samsung của bạn
          </h1>
        </div>
        <form className="flex flex-col mt-2 md:mt-5">
          <div className="relative my-7">
            <input
              type="text"
              id="first-name"
              autoComplete="off"
              className={`bg-transparent border-b-gray-400 border-b-2 w-full outline-none transition-colors peer focus:border-b-blue-500 focus:text-blue-500 ${
                firstName ? "text-gray-400" : "text-blue-500"
              }`}
              onChange={(e) => setFirstName(e.target.value)}
            />
            <label
              htmlFor="first-name"
              className={`absolute mb-1 cursor-text peer-focus:text-sm peer-focus:bottom-8 text-gray-400 peer-focus:text-blue-500 peer-focus:left-0 transition-all duration-300 ${
                firstName
                  ? "left-0 bottom-8 text-gray-500 text-sm"
                  : "left-1 bottom-1"
              }`}
            >
              Họ
            </label>
          </div>
          <p className="text-red-400 -mt-4 text-xs italic">
            {validatorMess?.firstName}
          </p>
          <div className="relative my-7">
            <input
              type="text"
              id="last-name"
              autoComplete="off"
              className={`bg-transparent  border-b-gray-400 border-b-2 w-full outline-none transition-colors peer focus:border-b-blue-500 focus:text-blue-500 ${
                lastName ? "text-gray-400" : "text-blue-500"
              }`}
              onChange={(e) => setLastName(e.target.value)}
            />
            <label
              htmlFor="last-name"
              className={`absolute cursor-text mb-1 peer-focus:text-sm peer-focus:bottom-8 text-gray-400 peer-focus:text-blue-500 peer-focus:left-0 transition-all duration-300 ${
                lastName
                  ? "left-0 bottom-8 text-gray-500 text-sm"
                  : "left-1 bottom-1"
              }`}
            >
              Tên
            </label>
          </div>
          <p className="text-red-400 -mt-4 text-xs italic">
            {validatorMess?.lastName}
          </p>
          <div className="relative my-7">
            <input
              type="email"
              id="email"
              autoComplete="off"
              className={`bg-transparent border-b-gray-400 border-b-2 w-full outline-none transition-colors peer focus:border-b-blue-500 focus:text-blue-500 ${
                email ? "text-gray-400" : "text-blue-500"
              }`}
              onChange={(e) => setEmail(e.target.value)}
            />
            <label
              htmlFor="email"
              className={`absolute cursor-text mb-1 peer-focus:text-sm peer-focus:bottom-8 text-gray-400 peer-focus:text-blue-500 peer-focus:left-0 transition-all duration-300 ${
                email
                  ? "left-0 bottom-8 text-gray-500 text-sm"
                  : "left-1 bottom-1"
              }`}
            >
              Email
            </label>
          </div>
          <p className="text-red-400 -mt-4 text-xs italic">
            {validatorMess?.email}
          </p>
          <div className="relative my-7">
            <input
              type={hiddenPassword}
              id="password"
              autoComplete="off"
              className={`bg-transparent  border-b-gray-400 border-b-2 w-full outline-none transition-colors peer focus:border-b-blue-500 focus:text-blue-500 ${
                password ? "text-gray-400" : "text-blue-500"
              }`}
              onChange={(e) => setPassword(e.target.value)}
            />
            <label
              htmlFor="password"
              className={`absolute cursor-text mb-1 peer-focus:text-sm peer-focus:bottom-8 text-gray-400 peer-focus:text-blue-500 peer-focus:left-0 transition-all duration-300 ${
                password
                  ? "left-0 bottom-8 text-gray-500 text-sm"
                  : "left-1 bottom-1"
              }`}
            >
              Mật khẩu
            </label>
            {hiddenPassword === "password" ? (
              <AiOutlineEyeInvisible
                className="absolute text-gray-400 right-10 cursor-pointer bottom-4 select-none"
                onClick={() => setHiddenPassword("text")}
              />
            ) : (
              <AiOutlineEye
                className="absolute right-10 cursor-pointer bottom-4 text-blue-500 select-none"
                onClick={() => setHiddenPassword("password")}
              />
            )}
          </div>
          <p className="text-red-400 -mt-4 text-xs italic">
            {validatorMess?.password}
          </p>
          <div className="relative my-7">
            <input
              type={hiddenPassword}
              id="password2"
              autoComplete="off"
              className={`bg-transparent  border-b-gray-400 border-b-2 w-full outline-none transition-colors peer focus:border-b-blue-500 focus:text-blue-500 ${
                password2 ? "text-gray-400" : "text-blue-500"
              }`}
              onChange={(e) => setPassword2(e.target.value)}
            />
            <label
              htmlFor="password2"
              className={`absolute cursor-text mb-1 peer-focus:text-sm peer-focus:bottom-8 text-gray-400 peer-focus:text-blue-500 peer-focus:left-0 transition-all duration-300 ${
                password2
                  ? "left-0 bottom-8 text-gray-500 text-sm"
                  : "left-1 bottom-1"
              }`}
            >
              Xác nhận mật khẩu
            </label>
          </div>
          <p className="text-red-400 -mt-4 text-xs italic">
            {validatorMess?.password2}
          </p>
        </form>
        <div className="mt-10 flex justify-evenly">
          <Link href="/">
            <button className="bg-gray-400 flex text-white md:px-20 px-5 w-fit text-lg rounded-full py-2 hover:bg-gray-500">
              Trở về
            </button>
          </Link>
          <button
            className="bg-blue-500 px-5 text-white md:px-20 text-lg rounded-full py-2 hover:bg-blue-600"
            onClick={handleRegister}
          >
            Đăng ký
          </button>
        </div>
        <div className="text-gray-400 items-center justify-center mt-5 text-base flex ">
          Bạn đã có tài khoản?{" "}
          <Link href="/login">
            <p className="underline ml-1 cursor-pointer text-blue-400 hover:text-blue-500">
              Đăng nhập
            </p>
          </Link>
        </div>
      </div>
    </>
  );
};

export default SignUp;
