/* eslint-disable react/no-unescaped-entities */
import Link from "next/link";
import React, { useState } from "react";
import { login } from "../redux/apiReq";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import { ReactElement } from "react";
import SignLayout from "../components/Layout/SignLayout";
import { CustomHeader } from "../components/Header/CustomHeader";
import { AiOutlineEyeInvisible, AiOutlineEye } from "react-icons/ai";

const SignIn = () => {
  const [email, setEmail] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [typePass, setTypePass] = useState("password");

  const router = useRouter();
  const dispatch = useDispatch();
  const errLogin = useSelector(
    (state: any) => state?.auth?.login.error?.response?.data
  );

  const handleLogin = async () => {
    try {
      const user = {
        email: email,
        password: password,
      };
      await login(user, dispatch, router);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      <CustomHeader>
        <title>Đăng nhập</title>
      </CustomHeader>
      <div className="bg-[#171717] w-4/5 xl:w-2/5 shadow-2xl rounded-xl py-8 md:py-16 text-xl px-5 md:px-12 flex flex-col justify-center z-50">
        <div className="space-y-2">
          <h1 className="text-gray-200 text-2xl sm:font-medium sm:text-3xl text-center">
            Đăng nhập Samsung account của bạn
          </h1>
        </div>
        <form className="flex flex-col space-y-10 mt-14">
          <div className="relative">
            <input
              type="email"
              id="email"
              autoComplete="off"
              className={`bg-transparent border-b-gray-400 border-b-2 w-full outline-none transition-colors peer focus:border-b-blue-500 focus:text-blue-500 ${
                email ? "text-gray-400" : "text-blue-500"
              }`}
              onChange={(e) => setEmail(e?.target?.value)}
            />
            <label
              htmlFor="email"
              className={`absolute mb-1 cursor-text peer-focus:text-sm peer-focus:bottom-8 text-gray-400 peer-focus:text-blue-500 peer-focus:left-0 transition-all duration-300 ${
                email
                  ? "left-0 bottom-8 text-gray-500 text-sm"
                  : "left-1 bottom-1"
              }`}
            >
              Email
            </label>
          </div>
          <div className="relative">
            <input
              type={typePass}
              id="password"
              autoComplete="off"
              className={`bg-transparent  border-b-gray-400 border-b-2 w-full outline-none transition-colors peer focus:border-b-blue-500 focus:text-blue-500 ${
                password ? "text-gray-400" : "text-blue-500"
              }`}
              onChange={(e) => setPassword(e?.target?.value)}
            />
            <label
              htmlFor="password"
              className={`absolute cursor-text mb-1 peer-focus:text-sm peer-focus:bottom-8 text-gray-400 peer-focus:text-blue-500 peer-focus:left-0 transition-all duration-300 ${
                password
                  ? "left-0 bottom-8 text-gray-500 text-sm"
                  : "left-1 bottom-1"
              }`}
            >
              Mật khẩu
            </label>
            {typePass === "password" ? (
              <AiOutlineEyeInvisible
                className="absolute text-gray-400 right-10 select-none cursor-pointer bottom-3"
                onClick={() => setTypePass("text")}
              />
            ) : (
              <AiOutlineEye
                className="absolute text-blue-500 select-none right-10 cursor-pointer bottom-3"
                onClick={() => setTypePass("password")}
              />
            )}
          </div>
        </form>
        <i className="text-red-500 text-base ml-1 mt-2">{errLogin}</i>
        <div className="mt-10 flex justify-evenly ">
          <Link href="/">
            <button
              type="button"
              className="bg-gray-400 flex text-white md:px-20 px-5 w-fit text-lg rounded-full py-2 hover:bg-gray-500"
            >
              <p>Trở về</p>
            </button>
          </Link>
          <button
            className="bg-blue-500 px-5 text-white md:px-20 text-lg rounded-full py-2 hover:bg-blue-600"
            onClick={handleLogin}
          >
            Đăng nhập
          </button>
        </div>

        <div className="text-gray-400 items-center justify-center mt-5 text-base flex ">
          Bạn chưa có tài khoản?{" "}
          <Link href="/register">
            <p className="underline ml-1 cursor-pointer text-blue-400 hover:text-blue-500">
              Đăng ký
            </p>
          </Link>
        </div>
      </div>
    </>
  );
};

SignIn.getLayout = function getLayout(page: ReactElement) {
  return <SignLayout>{page}</SignLayout>;
};

export default SignIn;
